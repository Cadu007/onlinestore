<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Customer Routes
|--------------------------------------------------------------------------
*/
Route::post( "auth/customer/login", "\App\Http\Controllers\Api\Auth\CustomerAuth@login" );
Route::post( "auth/customer/register", "\App\Http\Controllers\Api\Auth\CustomerAuth@register" );
Route::post( "auth/customer/update", "Admin\Customers\CustomerController@updateUser" );
Route::post( "auth/customer/message", "Api\Auth\MessageController@message" );

/*
|--------------------------------------------------------------------------
| Customer Profile Routes
|--------------------------------------------------------------------------
*/
Route::post( "customer/change_password", "Api\User\ChangePasswordController@ChangePassword" );


/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
*/
Route::get('/product/{id}', 'Api\Product\ProductController@show');
// Route::get('/product/{id}/reviews', 'Api\Product\ProductController@getProductReview');
// Route::post('/product/{id}/reviews', 'Api\Product\ProductController@setProductReview');
Route::get('/products','Api\Product\ProductController@index');
Route::get('/products/popular','Api\Product\ProductController@getPopularProduct');
Route::get('/products/new','Api\Product\ProductController@getNewProduct');
Route::post('/product/{id}/combination','Api\Product\ProductController@findProductCombination');
Route::get('/product/{id}/combination','Api\Product\ProductController@getProductAttribute');
/*
|--------------------------------------------------------------------------
| Categories Routes
|--------------------------------------------------------------------------
*/
Route::get('categories','Api\Category\CategoryController@index');
Route::get('categories/{id}/filtervalues','Api\Category\CategoryController@filterValues');
Route::get('categories/{id}/filter','Api\Category\CategoryController@filter');
Route::get('categories/{id}','Api\Category\CategoryController@show');
Route::get('categories/{id}/products','Api\Category\CategoryController@sort');

/*
|--------------------------------------------------------------------------
| Cart Routes
|--------------------------------------------------------------------------
*/
Route::get('cart', 'Api\Cart\CartController@index');
Route::post('cart', 'Api\Cart\CartController@store');
Route::delete('cart', 'Api\Cart\CartController@clear');
Route::put('cart/{id}', 'Api\Cart\CartController@update');
Route::delete('cart/{id}', 'Api\Cart\CartController@delete');

/*
|--------------------------------------------------------------------------
| Company Routes
|--------------------------------------------------------------------------
*/
Route::get('company/{id}','Api\Company\CompanyController@index');


/*
/--------------------------------------------------------
/ Banner Routes
/---------------------------------------------------------
*/
Route::get('banners','Admin\Banner\BannerController@index');
/*
/--------------------------------------------------------
/ Promotions Routes
/---------------------------------------------------------
*/
Route::get('promotions','Admin\Promotion\PromotionController@index');
/*
/--------------------------------------------------------
/ Question And Answer Routes
/---------------------------------------------------------
*/
Route::get('questionAnswer','Admin\QuestionAnswerController@allQA');


/*
/--------------------------------------------------------
/ Auction Routes
/---------------------------------------------------------
*/
Route::prefix('auction')->group(function(){

    Route::apiResource('product','Api\Auction\ProductController');
    Route::post('product/{id}/renew','Api\Auction\ProductController@renew');
    Route::get('products/trend','Api\Auction\ProductController@trend');
    Route::get('products/search','Api\Auction\ProductController@search');
    Route::get('products/filter','Api\Auction\ProductController@getFilterValues');
    Route::post('products/filter','Api\Auction\ProductController@filter');
    Route::prefix('product/{id}')->group(function()
    {
        Route::apiResource('bid','Api\Auction\BidController');
    });
    Route::get('products','Api\Auction\UserController@products');
    Route::get('bids','Api\Auction\UserController@bids');
});
Route::post('auction/product/image/delete','Api\Auction\ProductController@deleteImage');
Route::post('auction/product/{id}/image/add','Api\Auction\ProductController@addImage');
Route::post('auction/user','Api\Auction\UserController@store');

/*
/--------------------------------------------------------
/ Mall Routes
/---------------------------------------------------------
*/
Route::prefix('mall')->group(function()
{
    Route::apiResource('mall','Api\Mall\MallController');
    Route::prefix('mall/{id}')->group(function(){
        Route::apiResource('store','Api\Mall\StoreController');
        Route::prefix('store/{storeId}')->group(function()
        {
            Route::apiResource('product','Api\Mall\ProductController');
        });
    });
});

Route::post('product/{id}/create/review','Api\Review\ReviewController@addReview');