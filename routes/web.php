<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::group(['prefix' => 'company', 'middleware' => ['company'], 'as' => 'company.' ], function () {
    Route::namespace('Company')->group(function () {
        Route::get('/','CompanyController@index')->name('dashboard');
        Route::namespace('Brands')->group(function()
        {
            Route::resource('brands', 'BrandController');
        });
        Route::namespace('Orders')->group(function()
        {
            Route::resource('orders', 'OrderController');
        });
        Route::namespace('Products')->group(function () {
            Route::resource('products', 'ProductController');
            Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
            Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
        });
    });
});

Route::get('company/view-profile/{id}','Admin\Company\CompanyController@show');
Route::get('company/edit-profile/{id}/edit','Admin\Company\CompanyController@edit');
Route::patch('company/{id}','Admin\Company\CompanyController@update');
Route::get('company/change-password/{id}','Admin\Company\CompanyController@changePassword');
Route::patch('company/update-password/{id}','Admin\Company\CompanyController@updatePassword');
Route::get('company/addresses/{id}','Admin\Company\CompanyController@showAddresses');
Route::get('company/address/create','Admin\Addresses\AddressController@createCompanyAddress');
Route::post('company/address/{id}','Admin\Addresses\AddressController@storeCompanyAddress');
Route::get('company/register','Admin\Company\CompanyController@create');
Route::post('company','Admin\Company\CompanyController@store');

Route::get('questionAnswer', 'Admin\QuestionAnswerController@index');
Route::get('questionAnswer/create', 'Admin\QuestionAnswerController@create');
Route::post('questionAnswer/store', 'Admin\QuestionAnswerController@store');
Route::get('questionAnswer/edit/{id}', 'Admin\QuestionAnswerController@edit');
Route::patch('questionAnswer/update/{id}', 'Admin\QuestionAnswerController@update');
Route::delete('questionAnswer/delete/{id}', 'Admin\QuestionAnswerController@destroy');

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::namespace('Products')->group(function () {
                Route::resource('products', 'ProductController');
                Route::get('requests','ProductController@requests')->name('products.requests');
                Route::get('products/requests/{id}','ProductController@approve')->name('products.requests.approve');
                Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
                Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
            });
            Route::namespace('Customers')->group(function () {
                Route::resource('customers', 'CustomerController');
                Route::resource('customers.addresses', 'CustomerAddressController');
            });
            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            });
            Route::namespace('Company')->group(function () {
                Route::get('acceptCompany', 'CompanyController@acceptCompany')->name('company.acceptCompany');
                Route::patch('approveCompany/{id}', 'CompanyController@approveCompany')->name('company.approveCompany');
                Route::get('deactivateCompany', 'CompanyController@unauthorizedCompany')->name('company.unauthorizedCompany');
                Route::patch('deactivateCompany/{id}', 'CompanyController@unApproveCompany')->name('company.unApproveCompany');
            });
            Route::namespace('Banner')->group(function () {
                Route::get('createBanner', 'BannerController@createBanner')->name('banner.createBanner');
                Route::post('addBanner', 'BannerController@addBanner')->name('banner.addBanner');
            });
            Route::namespace('Review')->group(function () {
                Route::get('reviews/accept', 'ReviewController@accept')->name('reviews.accept');
                Route::get('reviews/approve/{id}', 'ReviewController@approve')->name('reviews.approve');
            });
            Route::namespace('Auction')->group(function () {
                Route::get('auction', 'AuctionController@index')->name('auction.index');
                Route::get('auction/approved', 'AuctionController@approvedAuction')->name('auction.approved');
                Route::get('auction/unapproved', 'AuctionController@unapprovedAuction')->name('auction.unapproved');
                Route::get('auction/approve/{id}', 'AuctionController@approve')->name('auction.approve');
            });
           
            Route::namespace('Promotion')->group(function () {
                Route::get('editPromotion', 'PromotionController@editPromotion')->name('promotion.editPromotion');
                Route::put('updatePromotion', 'PromotionController@updatePromotion')->name('promotion.updatePromotion');
            });
            Route::namespace('Orders')->group(function () {
                Route::resource('orders', 'OrderController');
                Route::resource('order-statuses', 'OrderStatusController');
                Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
            });
            Route::resource('addresses', 'Addresses\AddressController');
            Route::resource('countries', 'Countries\CountryController');
            Route::resource('countries.provinces', 'Provinces\ProvinceController');
            Route::resource('countries.provinces.cities', 'Cities\CityController');
            Route::resource('couriers', 'Couriers\CourierController');
            Route::resource('attributes', 'Attributes\AttributeController');
            Route::resource('attributes.values', 'Attributes\AttributeValueController');
            Route::resource('brands', 'Brands\BrandController');

        });
        Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('employees', 'EmployeeController');
            Route::get('employees/{id}/profile', 'EmployeeController@getProfile')->name('employee.profile');
            Route::put('employees/{id}/profile', 'EmployeeController@updateProfile')->name('employee.profile.update');
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        });
    });
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });

        Route::get('accounts', 'AccountsController@index')->name('accounts');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
    });
    Route::resource('cart', 'CartController');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('front.category.slug');
    Route::get("search", 'ProductController@search')->name('search.product');
    Route::get("{product}", 'ProductController@show')->name('front.get.product');

});