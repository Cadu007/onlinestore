<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register errors and success structures
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data) {
            return Response::json([
                'success'  => true,
                'data' => $data,
            ]);
        });
        Response::macro('fail', function ( $errors, $status = 400 ) {
            return Response::json( [
                'success'  => false,
                'errors' => $errors,
            ], $status);
        });
    }
}