<?php

namespace App\Providers;

use App\Shop\Companies\Company;
use App\Shop\Customers\Customer;
use Laravel\Cashier\Cashier;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cashier::useCurrency(config('cart.currency'), config('cart.currency_symbol'));
        Relation::morphMap([
            'company' => Company::class,
            'customer' => Customer::class,
        ]);
        app()->bind(
            'App\Shop\Companies\Repositories\Interfaces\CompanyRepositoryInterface',
            'App\Shop\Companies\Repositories\CompanyRepository'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
