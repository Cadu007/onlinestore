<?php

namespace App\Services\Authentication\Customer ;

use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Registration
{

    /**
     * @param $fields
     * @return Customer
     */
    public static function performRegistration( $fields )
    {
        $data = [
            'name' => $fields['name'],
            'email' => $fields['email'],
            'registration_type' => $fields['type'],
            'phone' => $fields['phone'],
            'country_code' => $fields['phone_country'],
            'device_id' => $fields['device_id'],
        ] ;

        if( $fields['type'] == Customer::REGISTRATION_TYPES['normal'] )
        {
            $data['password'] = Hash::make( $fields['password'] );
        }

        return Customer::create( $data );
    }

    /**
     * @param $data
     *
     * @return int
     */
    public static function register( $data )
    {
        $customer = self::performRegistration( $data ) ;

        return Auth::guard('customers')->login( $customer ) ;
    }
}
