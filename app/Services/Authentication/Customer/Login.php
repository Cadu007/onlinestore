<?php

namespace App\Services\Authentication\Customer ;

use App\Exceptions\UserNotFoundException ;
use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\Auth;

class Login
{
    public static function login( $data )
    {
        $email = $data['email'];
        $type = $data['type'];
        $token = '' ;

        switch ( $type )
        {
            case Customer::REGISTRATION_TYPES['normal']:

                $token = self::loginByPassword( $email, $data['password'] );

                break ;

            default:

                $token = self::loginSocial( $email, $type );

                break ;
        }

        return $token ;
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     * @throws UserNotFoundException
     */
    public static function loginByPassword( $email, $password )
    {
        $credentials = ['email' => $email, 'password' => $password ];

        if( ! $token = Auth::guard('customers')->attempt( $credentials ) )
        {
            throw new UserNotFoundException( __('auth.failed' ) );
        }

        return $token ;
    }

    /**
     * @param $email
     * @param $type
     * @return string
     * @throws UserNotFoundException
     */
    public static function loginSocial( $email, $type )
    {
        $customer = Customer::where('email', $email )->first();

        if( ! $customer )
        {
            throw new UserNotFoundException( __('auth.failed' ) );
        }

        if( $customer->registration_type !== $type )
        {
            throw new UserNotFoundException( __('auth.failed' ) );
        }

        return Auth::guard('customers')->login( $customer ) ;
    }
}
