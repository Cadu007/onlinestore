<?php

namespace App\Services\Authentication\Company ;

use App\Company;
use App\User;

class Registration
{
    const Mutations = [
        'country_code' => 'phone_country'
    ];

    /**
     * @param $data
     * @return array
     */
    public static function mutateFields( array $data )
    {
        $newData = $data ;

        foreach ( self::Mutations as $add => $remove )
        {
            $newData[$add] = $data[$remove];

            unset( $newData[$remove] );
        }

        return $newData ;
    }

    /**
     * @param $fields
     * @return User
     */
    public static function performNormalRegistration( $fields )
    {
        return User::create( $fields );
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public static function register( $data )
    {
        $fields = self::mutateFields( $data );

        $user = self::performNormalRegistration( $fields ) ;

        $company = Company::create( [ 'user_id' => $user->id ] );

        return $company->id ;
    }
}
