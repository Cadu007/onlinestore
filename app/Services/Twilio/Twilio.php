<?php

namespace App\Services\Twilio ;

use App\Shop\Phone\Phone;
use Twilio as TwilioClient;

class Twilio
{
    /**
     * @param $code
     * @return string
     */
    private static function buildMessageWithCode( $code ): string
    {
        return sprintf( 'Your Code is: %s', $code ) ;
    }

    /**
     * @param $phone
     * @param $phone_country
     */
    public static function createPhoneRecord( $phone ): void
    {
        $code = Phone::generateCode();

        Phone::savePhoneCode( $phone, $code );

        self::sendToPhone( $phone, self::buildMessageWithCode( $code ) );
    }

    /**
     * @param $phone
     * @param $message
     */
    private static function sendToPhone( $phone, $message ): void
    {
        TwilioClient::message( $phone, $message );
    }
}
