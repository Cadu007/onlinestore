<?php

namespace App\Shop\Companies\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CompanyNotFoundException extends NotFoundHttpException
{

    /**
     * CompanyNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('Company not found.');
    }
}
