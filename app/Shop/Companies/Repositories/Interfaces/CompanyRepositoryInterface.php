<?php

namespace App\Shop\Companies\Repositories\Interfaces;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Companies\Company;
use Illuminate\Support\Collection;

interface CompanyRepositoryInterface extends BaseRepositoryInterface
{
    public function listCompanies(string $order = 'id', string $sort = 'desc'): Collection;

    public function createCompany(array $params) : Company;

    public function findCompanyById(int $id) : Company;

    public function updateCompany(array $params): bool;

    public function syncRoles(array $roleIds);

    public function listRoles() : Collection;

    public function hasRole(string $roleName) : bool;

    public function isAuthUser(Company $Company): bool;

    public function deleteCompany() : bool;
}
