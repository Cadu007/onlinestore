<?php

namespace App\Shop\Companies\Repositories;


use App\Shop\Companies\Company;
use App\Shop\Orders\Order;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\Companies\Exceptions\CompanyNotFoundException;
use App\Shop\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\ProductAttributes\Repositories\ProductAttributeRepository;
use App\Shop\Products\Repositories\ProductRepository;
use Jsdecena\Baserepo\BaseRepository;


class CompanyRepository extends BaseRepository implements CompanyRepositoryInterface
{

    /**
     * CompanyRepository constructor.
     *
     * @param Company $company
     */
    public function __construct( Company $company )
    {
        parent::__construct( $company );

        $this->model = $company;
    }

    /**
     * List all the Companies
     *
     * @param string $order
     * @param string $sort
     *
     * @return Collection
     */
    public function listCompanies(string $order = 'id', string $sort = 'desc'): Collection
    {
        return $this->all(['*'], $order, $sort);
    }

    /**
     * Create the Company
     *
     * @param array $data
     *
     * @return Company
     */
    public function createCompany( array $data ): Company
    {
        $data['password'] = Hash::make( $data['password'] );

        return $this->create( $data );
    }

    /**
     * Find the Company by id
     *
     * @param int $id
     *
     * @return Company
     */
    public function findCompanyById(int $id): Company
    {
        try {
            return $this->findOneOrFail($id);
        }
        catch (ModelNotFoundException $e)
        {
            throw new CompanyNotFoundException;
        }
    }

    /**
     * Update Company
     *
     * @param array $params
     *
     * @return bool
     */
    public function updateCompany(array $params): bool
    {
       
      
            return $this->update($params);
      }

    

    /**
     * @param array $roleIds
     */
    public function syncRoles(array $roleIds)
    {
        $this->model->roles()->sync($roleIds);
    }

    /**
     * @return Collection
     */
    public function listRoles(): Collection
    {
        return $this->model->roles()->get();
    }

    /**
     * @param string $roleName
     *
     * @return bool
     */
    public function hasRole(string $roleName): bool
    {
        return $this->model->hasRole($roleName);
    }

    /**
     * @param Company $Company
     *
     * @return bool
     */
    public function isAuthUser(Company $Company): bool
    {
        $isAuthUser = false;
        if (Auth::guard('Company')->user()->id == $Company->id) {
            $isAuthUser = true;
        }
        return $isAuthUser;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteCompany() : bool
    {
        return $this->delete();
    }

    public function findProducts()
    {
        return $this->model->products;
    }

    public function findOrders(ProductRepository $productRepo)
    {
        $products = $this->model->products->pluck('id');
        $data = DB::table('order_product')->select()->whereIn('product_id',$products)->get();
        $data->map(function($item) use($productRepo)
        {
            $productAttribute = ProductAttribute::find($item->product_attribute_id);
            $combination_string = null;
            $combinations = $productRepo->findProductCombination($productAttribute);
            foreach($combinations as $key => $combination)
            {
                $combination_string = ' '.$combination_string.$combination.' ';
            }
            $item->product_combination = $combination_string;
            return $item;
        });
        return $data;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCommercialLicenceImage( UploadedFile $file ) : string
    {
        return $this->saveImage( $file, 'companies' );
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveNationalIdImage( UploadedFile $file ) : string
    {
        return $this->saveImage( $file, 'companies' );
    }

    /**
     * @param UploadedFile $file
     * @param string $location
     * @return string
     */
    protected function saveImage( UploadedFile $file, string $location ): string
    {
        return $file->store( $location, [ 'disk' => 'public' ] );
    }
}
