<?php

namespace App\Shop\Companies\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:4|unique:company',
            'email'=>'required|email|unique:company',
            'password'=>'required|min:6|confirmed',
            'phone'=>'required',
            'country_code'=>'required|min:2',
            'commercial_licence'=>'required|file|image:png,jpeg,jpg,gif',
            'national_id'=>'required|file|image:png,jpeg,jpg,gif',
            'logo'=>'required|file|image:png,jpeg,jpg,gif'
        ];
    }
}