<?php

namespace App\Shop\Companies;

use App\Banner;
use App\Shop\Addresses\Address;
use App\Shop\Brands\Brand;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class Company extends Authenticatable
{
    use Notifiable, SoftDeletes, LaratrustUserTrait;

    protected $table = 'company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'phone',
        'country_code',
        'commercial_licence',
        'national_id',
        'logo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function brands()
    {
        return $this->hasMany(Brand::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class,'company_id');
    }
    
    public function addresses()
    {
        return $this->morphMany(Address::Class , 'user');
    }

    public function banner()
    {
        return $this->belongsTo(Banner::class);
    }

    /**
     * @param string $image
     * @return string
     */
    protected function getCompanyImage( string $image )
    {
        return asset("storage/$image") ;
    }

    /**
     * @return string
     */
    public function getNationalIdImageUrl()
    {
        return $this->getCompanyImage( $this->national_id ) ;
    }

    /**
     * @return string
     */
    public function getCommercialLicenceUrl()
    {
        return $this->getCompanyImage( $this->commercial_licence ) ;
    }

    /**
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->getCompanyImage( $this->logo ) ;
    }
}
