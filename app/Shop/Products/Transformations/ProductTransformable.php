<?php

namespace App\Shop\Products\Transformations;

use App\Shop\Products\Product;
use App\Shop\Companies\Company;
use App\Shop\Brands\Brand;
use Illuminate\Support\Facades\Storage;

trait ProductTransformable
{
    /**
     * Transform the product
     *
     * @param Product $product
     * @return Product
     */
    protected function transformProduct(Product $product)
    {
        $prod = new Product;
        $prod->id = (int) $product->id;
        $prod->name = $product->name;
        $prod->sku = $product->sku;
        $prod->slug = $product->slug;
        $prod->description = $product->description;
        $prod->cover = asset("storage/$product->cover");
        $prod->quantity = $product->quantity;
        $prod->price = $product->price;
        $prod->new = $product->getBuyableNewness();
        $prod->in_stock = $product->getBuyableAvailability();
        $prod->status = $product->status;
        $prod->rating = $product->rating;
        $prod->distance_unit = $product->distance_unit;
        $prod->weight = (float) $product->weight;
        $prod->mass_unit = $product->mass_unit;
        $prod->sale_price =  $product->sale_price;
        $prod->brand_id = (int) $product->brand_id;
        $prod->category_id = (int) $product->category_id;
        $prod->company_id = (int) $product->company_id;
        $prod->brand_name = Brand::find($product->brand_id)->name;
        $prod->company_name = Company::find($product->company_id)->name;
        $prod->created_at = $product->created_at;
        $prod->length = $product->length;
        $prod->height = $product->height;
        $prod->width = $product->width;


        return $prod;
    }
}
