<?php

namespace App\Shop\Products\Repositories;

use App\Http\Resources\Product\Images\ImagesResource;
use App\Shop\Attributes\Attribute;
use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Customers\Customer;
use App\Shop\Products\Exceptions\ProductCreateErrorException;
use App\Shop\Products\Exceptions\ProductUpdateErrorException;
use App\Shop\Review\Repository\ReviewRepository;
use App\Shop\Review\Review;
use App\Shop\Tools\UploadableTrait;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Brands\Brand;
use App\Shop\Companies\Company;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\ProductImages\ProductImage;
use App\Shop\Products\Exceptions\ProductNotFoundException;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Transformations\ProductTransformable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    use ProductTransformable, UploadableTrait;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        parent::__construct($product);
        $this->model = $product;
    }

    /**
     * List all the products
     *
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return Collection
     */
    public function listProducts(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * Create the product
     *
     * @param array $data
     *
     * @return Product
     * @throws ProductCreateErrorException
     */
    public function createProduct(array $data) : Product
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new ProductCreateErrorException($e);
        }
    }

    /**
     * Update the product
     *
     * @param array $data
     *
     * @return bool
     * @throws ProductUpdateErrorException
     */
    public function updateProduct(array $data) : bool
    {
        $filtered = collect($data)->except('image')->all();

        try {
            return $this->model->where('id', $this->model->id)->update($filtered);
        } catch (QueryException $e) {
            throw new ProductUpdateErrorException($e);
        }
    }

    /**
     * Find the product by ID
     *
     * @param int $id
     *
     * @return Product
     * @throws ProductNotFoundException
     */
    public function findProductById(int $id) : Product
    {
        try {
            return $this->transformProduct($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new ProductNotFoundException($e);
        }
    }

    /**
     * Delete the product
     *
     * @param Product $product
     *
     * @return bool
     * @throws \Exception
     * @deprecated
     * @use removeProduct
     */
    public function deleteProduct(Product $product) : bool
    {
        $product->images()->delete();
        return $product->delete();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function removeProduct() : bool
    {
        return $this->model->where('id', $this->model->id)->delete();
    }

    /**
     * Return the categories which the product is associated with
     *
     * @return Collection
     */
    public function getCategories() : Collection
    {
        return $this->model->categories()->get();
    }

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        return $this->update(['cover' => null], $file['product']);
    }

    /**
     * @param string $src
     * @return bool
     */
    public function deleteThumb(string $src) : bool
    {
        return DB::table('product_images')->where('src', $src)->delete();
    }

    /**
     * Get the product via slug
     *
     * @param array $slug
     *
     * @return Product
     * @throws ProductNotFoundException
     */
    public function findProductBySlug(array $slug) : Product
    {
        try {
            return $this->findOneByOrFail($slug);
        } catch (ModelNotFoundException $e) {
            throw new ProductNotFoundException($e);
        }
    }

    /**
     * @param string $text
     * @return mixed
     */
    public function searchProduct(string $text) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchProduct($text);
        } else {
            return $this->listProducts();
        }
    }

    /**
     * @return mixed
     */
    public function findProductImages() : Collection
    {
        return ImagesResource::collection($this->model->images()->get())->collection;
    }

    /**
     * @return Collection
     */
    public function findCompany()
    {
        return $this->model->company;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('products', ['disk' => 'public']);
    }

    /**
     * @param Collection $collection
     *
     * @return void
     */
    public function saveProductImages(Collection $collection)
    {
        $collection->each(function (UploadedFile $file) {
            $filename = $this->storeFile($file);
            $productImage = new ProductImage([
                'product_id' => $this->model->id,
                'src' => $filename
            ]);
            $this->model->images()->save($productImage);
        });
    }

    /**
     * Associate the product attribute to the product
     *
     * @param ProductAttribute $productAttribute
     * @return ProductAttribute
     */
    public function saveProductAttributes(ProductAttribute $productAttribute) : ProductAttribute
    {
        $this->model->attributes()->save($productAttribute);
        return $productAttribute;
    }

    /**
     * List all the product attributes associated with the product
     *
     * @return Collection
     */
    public function listProductAttributes() : Collection
    {
        return $this->model->attributes()->get();
    }



    /**
     * Delete the attribute from the product
     *
     * @param ProductAttribute $productAttribute
     *
     * @return bool|null
     * @throws \Exception
     */
    public function removeProductAttribute(ProductAttribute $productAttribute) : ?bool
    {
        return $productAttribute->delete();
    }

    /**
     * @param ProductAttribute $productAttribute
     * @param AttributeValue ...$attributeValues
     *
     * @return Collection
     */
    public function saveCombination(Product $product,ProductAttribute $productAttribute, AttributeValue ...$attributeValues) : Collection
    {
        $category = $product->categories;
        return collect($attributeValues)->each(function (AttributeValue $value) use ($category,$productAttribute,$product) {
            return $productAttribute->attributesValues()->save($value,['product_id' => $product->id , 'attribute_id' => $value->attribute->id, 'category_id' => $category->id, 'brand_id' => $product->brand->id]);
        });
    }

    /**
     * @return Collection
     */
    public function listCombinations() : Collection
    {
        return $this->model->attributes()->get()->map(function (ProductAttribute $productAttribute) {
            return $productAttribute->attributesValues;
        });
    }

    public function getProductAttributeId($id , Request $request)
    {
        $data = $request->data;
        $attribute_values = [];
        foreach($data as $attrbiute_value)
        {
            array_push($attribute_values, $attrbiute_value);
        }
        foreach($attribute_values as $attribute_value)
        {
            $query = DB::table('attribute_value_product_attribute')->select(['product_attribute_id'])
            ->where('attribute_value_id',$attribute_value)->where('product_id',$id);
            $productAttributes = $query->pluck('product_attribute_id');
        }
        return $productAttributes[0];
    }


    public function findCombinations($id , Request $request)
    {
        $data = $request->data;
        $attribute_values = [];
        foreach($data as $attrbiute_value)
        {
            array_push($attribute_values, $attrbiute_value);
        }
        foreach($attribute_values as $attribute_value)
        {
            $query = DB::table('attribute_value_product_attribute')->select(['product_attribute_id'])
            ->where('attribute_value_id',$attribute_value)->where('product_id',$id);
            $productAttributes = $query->pluck('product_attribute_id');
        }
        $query = DB::table('attribute_value_product_attribute')->select('attribute_value_id')->where('product_id',$id)
        ->where('attribute_id',$request->attribute_id)->whereIn('product_attribute_id',$productAttributes);
        $attributes_values = $query->get(['*']);
        $attrs = [];
        foreach($attributes_values as $attribte_value)
        {
            $attribute_value = AttributeValue::find($attribte_value->attribute_value_id);
            $data = 
            [
                'id' => $attribute_value->id,
                'value' => $attribute_value->value
            ];
            array_push($attrs,$data);
        }
        return $attrs;
    }

    public function listCombinationValues() : Collection
    {
        $return = [];
        $productAttributes = $this->listProductAttributes();

        foreach ($productAttributes as $productAttribute)
        {
            $data = [
                'id' => $productAttribute->id,
                'quantity' => $productAttribute->quantity,
                'price' => $productAttribute->price,
                'sale_price'=>$productAttribute->sale_price,
                'default' => $productAttribute->default
            ];

            $productAttr = ProductAttribute::find($data['id']);
            $attrValues = $productAttr->attributesValues()->get();

            foreach ($attrValues as $attrValue)
            {
                $attribute = Attribute::find($attrValue->attribute_id);

                $dataAttrValue = [
                    'id' => $attrValue->id,
                    'value' => $attrValue->value,
                    'name' => $attribute->name
                ];

                $data['attributes'][] = $dataAttrValue;
            }

            array_push($return,$data);
        }
        return collect($return);
    }
    /**
     * @param ProductAttribute $productAttribute
     * @return \Illuminate\Database\Eloquent\Collection
     */


    public function findProductCombination(ProductAttribute $productAttribute)
    {
        $values = $productAttribute->attributesValues()->get();

        return $values->map(function (AttributeValue $attributeValue) {
            return $attributeValue;
        })->keyBy(function (AttributeValue $item) {
            return strtolower($item->attribute->name);
        })->transform(function (AttributeValue $value) {
            return $value->value;
        });
    }

    /**
     * @param Brand $brand
     */
    public function saveBrand(Brand $brand)
    {
        $this->model->brand()->associate($brand);
    }

    /**
     * @return Brand
     */
    public function findBrand()
    {
        if($this->model->brand != null)
        {
            return $this->model->brand->only(['id','name']);
        }
        return $this->model->brand;
    }


    /**
     * @return Illuminate\Database\Eloquent\Builder
     */

    public function getPopularProducts() : \Illuminate\Database\Eloquent\Builder
    {
        return Product::popular();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function getNewProducts() : \Illuminate\Database\Eloquent\Builder
    {
        return Product::new();
    }

    public function listAttributes()
    {
        $prod_attributes = [];
        $selectedIds = [];
        $categories  = $this->getCategories();
        foreach ($categories as $category)
        {
            $category = Category::find($category->id);
            $categoryAttributes = $category->attributes()->get();
            foreach ($categoryAttributes as $categoryAttribute)
            {
                if(! in_array($categoryAttribute->id,$selectedIds))
                {
                    $query = DB::table('attribute_value_product_attribute')->select(['attribute_value_id'])->where('attribute_id',$categoryAttribute->id)
                    ->where('product_id',$this->model->id);
                    $attr_values = $query->get()->pluck('attribute_value_id');
                    $attrs = [];
                    foreach($attr_values as $attr_value)
                    {
                        $item = AttributeValue::find($attr_value);  
                        array_push($attrs,$item->only(['id','value']));
                    }
                    $data = [
                        'id' => $categoryAttribute->id,
                        'name' => $categoryAttribute->name,
                        'values' => $attrs
                    ];
                    $data = collect($data);
                    array_push($selectedIds,$data['id']);
                    array_push($prod_attributes,$data);
                }
            }
        }
        return $prod_attributes;
    }

    /**
     * @return Collection
     */
    public function getReview() : Collection
    {
        $reviewsList = [];
        $reviews =  $this->model->Reviews()->get();
        foreach ($reviews as $review)
        {
            $data = [
                'id' => $review->id,
                'rate' => $review->rate,
                'content' => $review->content,
                'date'=> $review->created_at->diffForHumans()
            ];
            $customer = Customer::find($review->customer_id);
            $data['customer'] = [
                'name' => $customer->name
            ];
            array_push($reviewsList,$data);
        }
        return  collect($reviewsList);
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function createReview(array  $data)
    {

        $review = Review::create($data);
        $count = $this->model->Reviews()->count();
        $sum = $this->model->Reviews()->sum('rate');
        $this->model->rating = $sum / $count;
        $this->model->save();
        return $review;
    }


}
