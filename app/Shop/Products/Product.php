<?php

namespace App\Shop\Products;

use App\Shop\Review;
use App\Shop\Brands\Brand;
use App\Shop\Categories\Category;
use App\Shop\Companies\Company;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\ProductImages\ProductImage;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpParser\Builder;

class Product extends Model implements Buyable
{
    use SearchableTrait;

    public const MASS_UNIT = [
        'OUNCES' => 'oz',
        'GRAMS' => 'gms',
        'POUNDS' => 'lbs'
    ];

    public const DISTANCE_UNIT = [
        'CENTIMETER' => 'cm',
        'METER' => 'mtr',
        'INCH' => 'in',
        'MILIMETER' => 'mm',
        'FOOT' => 'ft',
        'YARD' => 'yd'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'products.name' => 10,
            'products.description' => 5
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku',
        'name',
        'description',
        'cover',
        'quantity',
        'price',
        'brand_id',
        'status',
        'weight',
        'mass_unit',
        'status',
        'sale_price',
        'length',
        'width',
        'height',
        'distance_unit',
        'slug',
        'category_id',
        'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Scope a query to only include popular products.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Query\Builder
     */

    public function scopePopular($query)
    {
        return $query->where('rating','>=','3');
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @return \Illuminate\Database\Query\Builder mixed
     */
    public function scopeNew($query)
    {
        $date = Carbon::now()->subDays(env('NEW_CRITERIA'));
        $now = Carbon::now();
        return $query->where('created_at','>=',$date)->where('created_at','<=',$now);
    }

    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @param null $options
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @param null $options
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }


    /**
     * Get the price of the Buyable item.
     *
     * @param null $options
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    /**
     * Get the newness of the Buyable item.
     *
     * @param null $options
     * @return bool
     */
    public function getBuyableNewness($options = null) : bool
    {
        $date = Carbon::parse($this->created_at);
        $diff = $date->diffInDays(Carbon::now());
        return $diff <= intval(env('NEW_CRITERIA'));
    }

    /**
     * Get the availability of the Buyable item.
     *
     * @param null $options
     * @return bool
     */
    public function getBuyableAvailability($options = null): bool
    {
        return $this->quantity > 0;
    }


    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @param string $term
     * @return Collection
     */
    public function searchProduct(string $term) : Collection
    {
        return self::search($term)->get();
    }

    /**
     * Search Product By Name
     * @param string $term
     * @return Collection
     */
    public static function searchProductByName(string $term) : Collection
    {
        $products = Product::where('slug','like','%'.$term.'%')->get();
        return $products;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Reviews() : HasMany
    {
        return $this->hasMany(Review\Review::class)->where('approved',1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

}
