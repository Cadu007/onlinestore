<?php

namespace App\Shop\PaymentMethods\Paytaps\Exceptions;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class PaytapsRequestError extends InvalidArgumentException
{
}
