<?php

namespace App\Shop\PaymentMethods\Paytaps\Repositories;

use App\Shop\Addresses\Address;
use App\Shop\Addresses\Repositories\AddressRepository;
use App\Shop\Carts\Repositories\CartRepository;
use App\Shop\Carts\ShoppingCart;
use App\Shop\Checkout\CheckoutRepository;
use App\Shop\PaymentMethods\Payment;
use App\Shop\PaymentMethods\Paytaps\Exceptions\PaytapsRequestError;
use Damas\Paytabs\paytabs;
use Illuminate\Http\Request;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Api\Payment as PayPalPayment;
use Ramsey\Uuid\Uuid;

class PaytapsRepository
{
    /**
     * @var mixed
     */
    private $paytaps;

    /**
     * PayPalExpressCheckoutRepository constructor.
     */
    public function __construct()
    {
        $paytaps = paytabs::getInstance( config('paytaps.merchant'), config('paytaps.secret') );

        $this->paytaps = $paytaps ;
    }

    /**
     * @return mixed
     */
    public function getApiContext()
    {
        return $this->paytaps;
    }

    /**
     * @param $shippingFee
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Shop\Addresses\Exceptions\AddressNotFoundException
     */
    public function process(Request $request)
    {
        $cartRepo = new CartRepository(new ShoppingCart());
        $items = $cartRepo->getCartItemsTransformed();

        $addressRepo = new AddressRepository(new Address());

        $billingAddress = $addressRepo->findAddressById($request->input('billing_address'));
        $this->payPal->setBillingAddress($billingAddress);

        if ($request->has('shipping_address')) {
            $shippingAddress = $addressRepo->findAddressById($request->input('shipping_address'));
            $this->payPal->setShippingAddress($shippingAddress);
        }

        $result = $this->paytaps->create_pay_page(array(
            "merchant_email" => config('paytaps.merchant'),
            'secret_key' => config('paytaps.secret'),
            'title' => "John Doe",
            'cc_first_name' => "John",
            'cc_last_name' => "Doe",
            'email' => "customer@email.com",
            'cc_phone_number' => "973",
            'phone_number' => "33333333",
            'billing_address' => "Juffair, Manama, Bahrain",
            'city' => "Manama",
            'state' => "Capital",
            'postal_code' => "97300",
            'country' => "BHR",
            'address_shipping' => "Juffair, Manama, Bahrain",
            'city_shipping' => "Manama",
            'state_shipping' => "Capital",
            'postal_code_shipping' => "97300",
            'country_shipping' => "BHR",
            "products_per_title"=> "Mobile Phone",
            'currency' => "BHD",
            "unit_price"=> "10",
            'quantity' => "1",
            'other_charges' => "0",
            'amount' => "10.00",
            'discount'=>"0",
            "msg_lang" => "english",
            "reference_no" => "1231231",
            "site_url" => "https://your-site.com",
            'return_url' => "https://www.mystore.com/paytabs_api/result.php",
            "cms_with_version" => "API USING PHP"
        ));

        if($result->response_code == 4012){
            return redirect($result->payment_url);
        }
        return $result->result;

        $this->payPal->setPayer();
        $this->payPal->setItems($items);
        $this->payPal->setOtherFees(
            $cartRepo->getSubTotal(),
            $cartRepo->getTax(),
            $shippingFee
        );
        $this->payPal->setAmount($cartRepo->getTotal(2, $shippingFee));
        $this->payPal->setTransactions();

        $billingAddress = $addressRepo->findAddressById($request->input('billing_address'));
        $this->payPal->setBillingAddress($billingAddress);

        if ($request->has('shipping_address')) {
            $shippingAddress = $addressRepo->findAddressById($request->input('shipping_address'));
            $this->payPal->setShippingAddress($shippingAddress);
        }

        try {

            $response = $this->payPal->createPayment(
                route('checkout.execute', $request->except('_token', '_method')),
                route('checkout.cancel')
            );

            $redirectUrl = config('app.url');
            if ($response) {
                $redirectUrl = $response->links[1]->href;
            }
            return redirect()->to($redirectUrl);
        } catch (PayPalConnectionException $e) {
            throw new PaytapsRequestError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @throws \Exception
     */
    public function execute(Request $request)
    {
        $payment = PayPalPayment::get($request->input('paymentId'), $this->payPal->getApiContext());
        $execution = $this->payPal->setPayerId($request->input('PayerID'));
        $trans = $payment->execute($execution, $this->payPal->getApiContext());

        $cartRepo = new CartRepository(new ShoppingCart);
        $transactions = $trans->getTransactions();

        foreach ($transactions as $transaction) {
            $checkoutRepo = new CheckoutRepository;
            $checkoutRepo->buildCheckoutItems([
                'reference' => Uuid::uuid4()->toString(),
                'courier_id' => 1,
                'customer_id' => $request->user()->id,
                'address_id' => $request->input('billing_address'),
                'order_status_id' => 1,
                'payment' => $request->input('payment'),
                'discounts' => 0,
                'total_products' => $cartRepo->getSubTotal(),
                'total' => $cartRepo->getTotal(),
                'total_paid' => $transaction->getAmount()->getTotal(),
                'tax' => $cartRepo->getTax()
            ]);
        }

        $cartRepo->clearCart();
    }
}
