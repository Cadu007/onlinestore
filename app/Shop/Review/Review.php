<?php

namespace App\Shop\Review;

use App\Shop\Customers\Customer;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Review extends Model
{
    //
    protected $fillable = ['product_id','customer_id','rate','content'];

    /**
     * @return BelongsTo
     */
    public function products() : BelongsTo
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    /**
     * @return BelongsTo
     */
    public function customers() : BelongsTo
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }

}
