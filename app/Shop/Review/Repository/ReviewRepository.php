<?php


namespace App\Shop\Review\Repository;


use App\Review;
use App\Shop\Customers\Customer;
use App\Shop\Products\Exceptions\ProductUpdateErrorException;
use App\Shop\Products\Product;
use Illuminate\Database\QueryException;
use Jsdecena\Baserepo\BaseRepository;
use Stripe\Collection;

class ReviewRepository extends BaseRepository
{

    public function __construct(Review $review)
    {
        parent::__construct($review);
        $this->model = $review;
    }

    public function listAllReviews(string $product_id) : Collection
    {
        $reviews =  $this->all()->where('product_id',$product_id);
        foreach ($reviews as $review)
        {
            $review->customer_id = Customer::find($review->customer_id)->get();
        }
        return  $reviews;
    }

    public function createReview(array $data) : Review
    {
        return $this->create($data);
    }

    public function updateReview(array $data) : bool
    {
        $filtered = collect($data)->except(['product_id','customer_id'])->all();
        return $this->model->where('id', $this->model->id)->update($filtered);
    }

    public function deleteReview(string $id)
    {
        return $this->delete();
    }
}