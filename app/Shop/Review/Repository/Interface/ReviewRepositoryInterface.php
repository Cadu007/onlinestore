<?php

namespace App\Shop\Review\Repository;


use App\Review;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use Stripe\Collection;

interface ProductRepositoryInterface extends BaseRepositoryInterface
{
    public function listAllReviews(string $product_id) : Collection;
    public function createReview(array $data) : Review;
    public function updateReview(array $data) : bool;
    public function deleteReview(string $id);

}
