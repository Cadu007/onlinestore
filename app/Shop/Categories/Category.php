<?php

namespace App\Shop\Categories;

use App\Auction\Product as AppProduct;
use App\Mall\Store;
use App\Shop\Attributes\Attribute;
use App\Shop\Products\Product;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use NodeTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'cover',
        'status',
        'parent_id',
        'f_color',
        'l_color',
        'icon_code',
        'icon_font'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function products()
    {
        return $this->hasMany(Product::class,'category_id');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'Attributes_Categories');
    }

    public function stores()
    {
        return $this->hasMany(Store::class);
    }
    public function auctionProduct()
    {
        return $this->hasMany(AppProduct::class ,'category_id');
    }
    public static function boot()
    {
        parent::boot();

        static::deleting(function($category)
        {
            $uncategorized = Category::firstOrCreate(['name' => 'UnCategorized']);
            Product::where('category_id', $category->id)->update([
                'category_id' => $uncategorized->id
            ]);
        });
    }
}
