<?php

namespace App\Shop\Categories\Repositories;

use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Product\ProductResource;
use App\Shop\Attributes\Attribute;
use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Categories\Category;
use App\Shop\Categories\Exceptions\CategoryInvalidArgumentException;
use App\Shop\Categories\Exceptions\CategoryNotFoundException;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Products\Product;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    use UploadableTrait, ProductTransformable;

    /**
     * CategoryRepository constructor.
     *
     * @param Category $category
     */
    public function __construct( Category $category )
    {
        parent::__construct( $category );
    }

    /**
     * List all the categories
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listCategories(string $order = 'id', string $sort = 'desc', $except = []): Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * List all root categories
     * 
     * @param  string $order 
     * @param  string $sort  
     * @param  array  $except
     * @return \Illuminate\Support\Collection  
     */
    public function rootCategories(string $order = 'id', string $sort = 'desc', $except = []): Collection
    {

        return $this->model->whereIsRoot()->orderBy($order, $sort)->get()->except($except);
    }

    /** 
     * Categories that will appear in filter fields
     * The Categories that only have auctions
     * @return array
     */
    public static function filteredCategories()
    {
        $categories = Category::all();
        return CategoryRepository::getCategoriesFilteredData($categories);
    }

    /**
     * Get All Categories Wether its Parent Or Sub With Specific Data Schema 
     * id,name,count
     * @param Collection $categories
     * @return array 
     */

    public static function getCategoriesFilteredData(Collection $categories)
    {
        $cats = [];
        $categories->map(function(Category $cat) use(&$cats)
        {
            $count = 0;
            $cat = new CategoryRepository($cat);
            if($cat->checkParent())
            {   
                $count = $cat->getParentAuctionCount();
            }
            else
            {
                $count = $cat->getAuctionCount();
            }
            if($count > 0)
            {
                $data = [
                    'id' => $cat->model->id,
                    'name' => $cat->model->name . ' (' . $count . ')'
                        ];
                array_push($cats,$data);
            }
        });
        return $cats;
    }

    /**
     * Get The Count Of Auctions that parent category have
     */

    public function getParentAuctionCount()
    {
        $count = 0;
        $children = $this->getChildren();
        foreach($children as $sub)
        {
            $sub = $this->findCategoryById($sub['id']);
            $sub = new CategoryRepository($sub);
            if($sub->getAuctionCount() > 0)
            {
                $count = $count +  $sub->getAuctionCount();
            }
        }
        return $count;
    }

    /**
     * Check If The Category Is A Parent Category
     * @return bool
     */
    public function checkParent()
    {
        if($this->model->parent_id == null)
        {
            return true;
        }
        return false;
    }
    /**
     * Get Count Of The Auctions That Belongs To This Category
     * @return int
     */
    public function getAuctionCount()
    {
        return $this->model->auctionProduct()->count();
    }

    public function listAttributesValues()
    {
        $attrs = [];
        if($this->model->parent_id  == null)
        {
           $catChildren = $this->getChildren();
           $selectedIds = [];
           foreach($catChildren as $cat)
           {
                $cat = Category::find($cat['id']);
                $catRepo = new CategoryRepository($cat);
                $attributes = $catRepo->listAttributes();
                foreach($attributes as $attribute)
                {
                    if(! in_array($attribute['id'],$selectedIds))
                    {
                        $attributeValue = Attribute::find($attribute['id'])->values->map(function($item)
                        {
                            $item = [
                                'id' => $item->id,
                                'value' => $item->value
                            ];
                            return $item;
                        });
                        $data = 
                        [
                            'id' => $attribute['id'],
                            'name' => $attribute['name'],
                            'values' => $attributeValue 
                        ];
                        array_push($attrs,$data);
                        array_push($selectedIds,$data['id']);
                    }
                }
           }
        }
        else
        {

            $attr = $this->listAttributes();
            foreach($attr as $attribute)
            {
                $values = Attribute::find($attribute['id'])->values->map(function($item)
                {
                    $item = [
                        'id' => $item->id,
                        'value' => $item->value
                    ];
                    return $item;
                });
                $data = 
                [
                    'id' => $attribute->id,
                    'name' => $attribute->name,
                    'value' => $values
                ];
                array_push($attrs,$data);
            }
        }
        return $attrs;
    }

    public function filter(Request $request)
    {
        $parameters = $request->all();
        $min_price = 0;
        $max_price = 1000000000000;
        $attrs = [];
        foreach($parameters as $key => $value)
        {
            if($key == 'min_price')
            {
                $min_price = $value;
            }
            else if($key == 'max_price')
            {
                $max_price = $value;
            }
            else if($key == 'brand')
            {
                $brands = explode(',',$value);
            }
            else
            {
                array_push($attrs,$value);
            }
        }
        $data = DB::table('attribute_value_product_attribute')->select('product_id')->whereIn('brand_id',$brands)
        ->where('category_id',$this->model->id)->whereIn('attribute_value_id',$attrs)->get()->pluck('product_id')->toArray();
        $products = Product::all()->whereIn('id',$data)->whereBetween('price',[$min_price,$max_price]);
        return $products;
    }

    public function getBrands()
    {
        $brands = [];
        $selectedIds = [];
        if($this->model->parent_id == null)
        {
            $products = $this->getChildrenProducts();
        }
        else
        {
            $products = $this->findProducts();
        }
        $products->each(function(Product $product) use (&$selectedIds,&$brands)
        {
            $brand = $product->brand->only(['id','name']);
            if(! in_array($brand['id'],$selectedIds))
            {
                array_push($brands,$brand);
                array_push($selectedIds,$brand['id']); 
            }
        });
        return $brands;
    }
    /**
     * Create the category
     *
     * @param array $params
     *
     * @return Category
     * @throws CategoryInvalidArgumentException
     * @throws CategoryNotFoundException
     */
    public function createCategory(array $params): Category
    {
        try {
            $params['f_color'] = str_replace('#','0xFF',$params['f_color']);
            $params['l_color'] = str_replace('#','0xFF',$params['l_color']);
            $collection = collect($params);
            if (isset($params['name'])) {
                $slug = str_slug($params['name']);
            }

            if (isset($params['cover']) &&  ($params['cover'] instanceof UploadedFile)) {
                $cover = $this->uploadOne($params['cover'], 'categories');
            }
            if(isset($params['cover']))
            {
                $merge = $collection->merge(compact('slug', 'cover'));
            }
            else {
                $merge = $collection->merge(compact('slug'));
            }

            $category = new Category($merge->all());

            if ( isset( $params['parent']) && $params['parent'] != 0  ) {
                $parent = $this->findCategoryById($params['parent']);
                $category->parent()->associate( $parent );
            }

            $categoryRepo = new CategoryRepository($category);
            $category->save();

            if(isset($params['Attr'])) {
                $categoryRepo->syncAttributes($params['Attr']);
            }

            return $category;
        }
        catch ( QueryException $e )
        {
            throw new CategoryInvalidArgumentException($e->getMessage());
        }
    }

    /**
     * Update the category
     *
     * @param array $params
     *
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function updateCategory(array $params): Category
    {
        $params['f_color'] = str_replace('#','0xFF',$params['f_color']);
        $params['l_color'] = str_replace('#','0xFF',$params['l_color']);
        $category = $this->findCategoryById($this->model->id);
        $categoryRepo = new CategoryRepository($category);
        $collection = collect($params)->except('_token');
        $slug = str_slug($collection->get('name'));

        if (isset($params['cover']) &&  ($params['cover'] instanceof UploadedFile)) {
            $cover = $this->uploadOne($params['cover'], 'categories');
        }
        if(isset($params['cover']))
        {
            $merge = $collection->merge(compact('slug', 'cover'));
        }
        else {
            $merge = $collection->merge(compact('slug'));
        }

        // set parent attribute default value if not set
        $params['parent'] = $params['parent'] ?? 0;

        // If parent category is not set on update
        // just make current category as root
        // else we need to find the parent
        // and associate it as child
        if ((int) $params['parent'] == 0) {
            $category->saveAsRoot();
        } else {
            $parent = $this->findCategoryById($params['parent']);
            $category->parent()->associate($parent);
        }

        $category->update($merge->all());

        if(isset($params['Attr'])) {
            $categoryRepo->syncAttributes($params['Attr']);
        }
        else{
            $categoryRepo->detachAttributes();
        }

        return $category;
    }

    /**
     * @param int $id
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function findCategoryById(int $id): Category
    {
        try {
            return $this->findOneOrFail($id);
        }
        catch (ModelNotFoundException $e)
        {
            throw new CategoryNotFoundException( $e->getMessage() );
        }
    }

    /**
     * Delete a category
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteCategory(): bool
    {
        return $this->model->delete();
    }

    /**
     * Associate a product in a category
     *
     * @param Product $product
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function associateProduct(Product $product)
    {
        return $this->model->products()->save($product);
    }

    /**
     * Return all the products associated with the category
     *
     * @return mixed
     */
    public function findProducts(): Collection
    {
        return $this->model->products;
    }


    /**
     * @param array $params
     */
    public function syncAttributes(array $params)
    {
        $this->model->attributes()->sync($params);
    }

    public function detachAttributes()
    {
        $this->model->attributes()->detach();
    }
    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null): bool
    {
        return $this->update(['cover' => null], $file['category']);
    }

    /**
     * Return the category by using the slug as the parameter
     *
     * @param array $slug
     *
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function findCategoryBySlug( array $slug ): Category
    {
        try {
            return $this->findOneByOrFail( $slug );
        } catch (ModelNotFoundException $e) {
            throw new CategoryNotFoundException($e);
        }
    }

    /**
     * @return mixed
     */
    public function findParentCategory()
    {
        return $this->model->parent;
    }

    /**
     * get child categoreis
     *
     * @return mixed
     */
    public function getChildren()
    {
        return $this->model->children->toArray();
    }

    /**
     * @param $id
     * @return Collection
     * @throws CategoryNotFoundException
     */
    public function getChildrenProducts()
    {
        $catProducts = [];
        $selectedChildren = $this->model->children->pluck('id')->toArray();
        foreach ($selectedChildren as $selectedChild) {
            $childCat = $this->findCategoryById($selectedChild);
            $products = $childCat->products;
            foreach ($products as $product) {
                array_push($catProducts, $product);
            }
        }
        return collect($catProducts);
    }


    /**
     * get category attributes's
     *
     * @return array
     */


    public function listAttributes()
    {
        return $this->model->attributes()->get();
    }



    public function allCategoryProduct($number)
    {
        return Product::paginate($number);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws CategoryNotFoundException
     */
    public function allSubcategoryProduct(int $id)
    {
        $category = self::findCategoryById( $id );

        return $category->products;
    }

    /**
     * Get sorting according to selected sort and order techniques
     *
     * @param int $id
     * @return Collection
     * @throws CategoryNotFoundException
     */
    public function sortProductForCategory(int $id)
    {
        $ordering_types = ['desc', 'asc'];
        $sorting_types = ['price', 'name', 'rating'];
        $default_sort = $sorting_types[0];
        $default_order = $ordering_types[0];

        $selected_order = request()->get('order');
        $selected_sort = request()->get('sort');

        if ( ! is_null( $selected_sort ) && in_array( $selected_sort, $sorting_types ) )
        {
            $default_sort = $selected_sort;
        }

        if ( ! is_null( $selected_order ) && in_array( $selected_order, $ordering_types ) )
        {
            $default_order = $selected_order;
        }

        if($this->model->parent_id == null)
        {
            $order = true;
            if($default_order == 'asc')
            {
                $order = false;
            }
            $sorted =   $this->getChildrenProducts()->sortBy($default_sort,0,$order);
            return  $sorted->values();
        }
        else
        {
            return $this->model->products()->orderBy( $default_sort, $default_order )->get();
        }
    }
}
