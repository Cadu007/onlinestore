<?php
namespace App\Shop\Phone;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Phone extends Model
{
    protected $table = 'phone_validates' ;

    /**
     * Generate Random Code
     *
     * @return string
     */
    public static function generateCode()
    {
        return Str::random(4);
    }
    /**
     * @param $phone
     * @return mixed
     */
    public static function getPhoneCode( $phone )
    {
        $phone_validator = self::where('phone', $phone )->first();

        return ! is_null( $phone_validator ) ? $phone_validator->code : false;
    }
    /**
     * @param $phone
     * @param $phone_code
     * @param $code
     */
    public static function savePhoneCode( $phone, $code ): void
    {
        $phone_validator = self::where('phone', $phone )->first();

        $phone_validator = is_null( $phone_validator ) ? new self() : $phone_validator ;
        $phone_validator->phone = $phone ;
        $phone_validator->code = $code ;
        $phone_validator->save();
    }
}