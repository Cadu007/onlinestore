<?php

namespace App;

use App\Shop\Companies\Company;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'company_id',
        'banner',
        'start_date',
        'end_date'
    ];

    // '\ssss'
    public function company()
    {
        return $this->hasOne(Company::class,'id','company_id');
    }
}
