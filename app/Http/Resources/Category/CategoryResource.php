<?php

namespace App\Http\Resources\Category;

use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        if($this->parent_id == null)
        {
           return [
                "id"=>$this->id,
                "name" => __(strtolower($this->name)),
                "slug"=> $this->slug,
                "status" => $this->status,
                "f_color"=> $this->f_color,
                "l_color"=> $this->l_color,
                "icon_code"=> $this->icon_code,
                "icon_font"=> $this->icon_font,
            ];
            
        }
        else{

            return [
                "id"=>$this->id,
                "name"=> __($this->name),
                "slug"=> $this->slug,
                "status"=>$this->status,
                "parent_id"=> $this->parent_id,
                "cover"=> asset("storage/$this->cover"),
            ];
        }

    }
}
