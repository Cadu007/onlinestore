<?php

namespace App\Http\Resources\Product;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\ProductRepository;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::find($this->id);
        $productRepo = new ProductRepository($product);
        if(! str_contains($this->cover,'http'))
        {
            $this->cover = asset("storage/$product->cover");
        }
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'sale_price' => $this->sale_price,
            'quantity' => $this->quantity,
            "new" => $product->getBuyableNewness(),
            "in_stock" => $product->getBuyableAvailability(),
            'cover' => $this->cover,
            'images' => $productRepo->findProductImages(),
            'brand' => $productRepo->findBrand(),
            'comapny' => $productRepo->findCompany()->only(['id','name']),
            'category'=>$product->categories->only(['id','name']),
            'category_attributes' => $productRepo->listAttributes(),
            'rating' =>  $this->rating,
            'reviews'=> $product->Reviews
        ];
    }
}
