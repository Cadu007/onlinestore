<?php

namespace App\Http\Resources\Mall;

use App\Shop\Categories\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $category = Category::find($this->category_id);
        return
        [
            'id' => $this->id,
            'name' => $this->name,
            'open_time' => $this->open_time,
            'close_time' => $this->close_time,
            'cover' => asset("storage/$this->cover"),
            'category' => $category->name,
            'location' => $this->location,
            'mobile' => $this->mobile
        ];
    }
}
