<?php

namespace App\Http\Resources\Mall;

use App\Mall\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::find($this->id);
        return 
        [
            'id' => $this->id,
            'name' => $this->name,
            'cover' => asset("storage/$this->cover"),
            'price' => $this->price,
            'description' => $this->description,
            'store' => $product->store
        ];
    }
}
