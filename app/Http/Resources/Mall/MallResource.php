<?php

namespace App\Http\Resources\Mall;

use Illuminate\Http\Resources\Json\JsonResource;

class MallResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'location'=>$this->location,
            'open_time'=>$this->open_time,
            'close_time'=>$this->close_time,
            'map'=>$this->map,
            'cover'=>asset($this->cover),
            'city'=>  $this->city . ', '. $this->country->name

        ];
    }
}
