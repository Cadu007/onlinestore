<?php

namespace App\Http\Resources\Auction;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        [
            'id' => $this->id,
            'name' => $this->name,
            'cover' => asset("storage/$this->cover"),
            'owner' => $this->checkUser($request->auctionUser),
            'bidders' => $this->bidders,
            'location' => $this->city .', '. $this->country,
            'price' => $this->price,
            'remaining_time' => $this->getRemainingTime()
        ];
    }
}
