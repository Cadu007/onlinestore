<?php

namespace App\Http\Resources\Auction;

use App\Auction\Product as AppProduct;
use App\Http\Resources\Product\Images\ImagesResource;
use App\Shop\Cities\City;
use App\Shop\Countries\Country;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->guard('customers')->user();
        // dd($user->id);
        // dd($this->user->parentUser->id);
        
        if($user != null && ($user->id == $this->user->parentUser->id))
        {
            $owner = true;
        }
        else{
            $owner = false;
        }
        $product = AppProduct::find($this->id);
        if($this->bidders == 0)
        {
            $bids = collect([]);
            $currentBid = $this->start_price;
        }
        else
        {
            $bids = BidResource::collection($product->bids)->collection;
            $currentBid = $product->currentBid()->amount;
        }
        return
        [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'start_price' => $this->start_price,
            'current_price' => $currentBid,
            'number_of_bidders' => $this->bidders,
            'end_date' => $product->end_date,
            // 'location' => Country::find($this->country_id)->name . ' , ' . $this->city,
            'location' => $this->country. ' , ' . $this->city,
            'min_increment' => $this->min_increment,
            // 'images' => ImagesResource::collection($product->images)->collection,
            'cover' => asset("storage/$this->cover") ,
            'owner'=>  $owner,
            'Bids' => $bids,
            'Category'=>$this->categories->name,
            'Images'=> ImagesResource::collection($this->images),
        ];
    }
}
