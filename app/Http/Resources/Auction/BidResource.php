<?php

namespace App\Http\Resources\Auction;

use App\Auction\Bid as AppBid;
use App\Auction\User;
use App\Shop\Addresses\Address;
use App\Shop\Countries\Country;
use App\Shop\Customers\Customer;
use Illuminate\Http\Resources\Json\JsonResource;

class BidResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $bid = AppBid::find($this->id);
        $auction_user = $bid->user;
        $customer = Customer::find($auction_user->user_id);
        $adderess = Address::where('user_id',$customer->id)->first();
        return 
        [
            'id' => $this->id,
            'avatar' => asset("storage/$customer->avatar"),
            'name' => $customer->name,
            'location' => $adderess != null ? Country::find($adderess->country_id)->name . ', ' . $adderess->city : null,
            'amount' => $this->amount
        ];
    }
}
