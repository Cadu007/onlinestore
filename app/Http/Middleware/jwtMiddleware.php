<?php

namespace App\Http\Middleware;

use App\Auction\User;
use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
class jwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user = auth('customers')->user();

        $request->auctionUser = User::where("user_id",$user->id)->first();

        if($request->auctionUser == null)
        {
            return response()->json(['success'=>'false','data' => [
                'message' => 'Sorry You have to create an auction account'
            ]]);
        }

        return $next($request);
    }
}
