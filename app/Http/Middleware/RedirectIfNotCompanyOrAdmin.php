<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotCompanyOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'company')
    {
        if (!auth()->guard( 'employee')->check() && ! auth()->guard('company')->check()) {
            $request->session()->flash('error', 'You must be a company or employee to see this page');
            return redirect(route('admin.login'));
        }

        return $next($request);
    }
}
