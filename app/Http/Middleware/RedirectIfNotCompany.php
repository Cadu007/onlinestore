<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'company')
    {
        if ( ! auth()->guard('company')->check() )
        {
            $request->session()->flash('error', 'You must be a company to see this page');

            return redirect(route('admin.login'));
        }

        return $next($request);
    }
}
