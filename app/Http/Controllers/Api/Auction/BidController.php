<?php

namespace App\Http\Controllers\Api\Auction;

use App\Auction\Bid;
use App\Auction\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Auction\BidResource;


class BidController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwtMiddleware',['store','destroy']);
    }
    /**
     * Create bid for product
     * @param Request
     * @param Product $id
     * @return Response
     * @author Amr Degheidy
     */
    public function store($id, Request $request)
    {
        $product = Product::find($id);
        $user = $request->auctionUser;
        // dd($user);
        $max = Bid::where('product_id',$id)->max('amount');  
        // dd($max);
        if(($request->amount <= $max) &&( $max != null))
        {
            return response()->json(['fail'=>'you cannot bid less than maximum'],401);
        }
        if($product->user_id == $user->id)
        {
            return response()->json(['success' => 'false','data' => ['message' => 'You Are Not Allowed']]);
        }
        else
        {
            $bid = Bid::create([
                'product_id' => $id,
                'user_id' => $request->auctionUser->id,
                'amount'  => $request->amount,
            ]);
            $product = Product::find($id);
            $product->bidders = $product->bidders + 1;
            $product->save();
            $currentBid = $bid; // this should be *this* bid
            // $users = $product->bids->pluck(['user_id']);
            // dd($users);
            $currentBid = new BidResource($currentBid);
            return response()->success($currentBid);
        }
     }

    //  public function destroy($productId, $bidId)
    //  {
    //      $bid = Bid::find($bidId);
    //      $bid->delete();
    //      return response()->success([]);
    //  }
}
