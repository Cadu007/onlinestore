<?php

namespace App\Http\Controllers\Api\Auction;

use App\Auction\Bid;
use App\Auction\Product;
use App\Auction\User as AppUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Auction\ProductResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class UserController extends Controller
{

    public function __construct()
    {

        $this->middleware('jwtMiddleware', ['only' => ['products','bids']]);
    }
    /**
     * store product
     * @param Request
     * @return Response
     * 
     *
     */
    public function store(Request $request)
    {   


            $auctionUser = new AppUser();
            $auctionUser->user_id = auth('customers')->user()->id;
            $check = AppUser::where('user_id','=',$auctionUser->user_id)->first();
           if($check != null)
           {
               return response()->json(['success'=>'you already registered'],401);
           }
            $validate = Validator::make($request->all(),[
                'national_id'=> 'string|unique:auction_users'
                
            ]);
            if($validate->fails())
            {
                return response()->json(['success'=>'National ID already Taken'],401);
            }
            
            $auctionUser->National_ID = $request->national_id;  
            $auctionUser->save();

            return response()->success([]);
    }
    /**
     * to get user auctions or products
     * @return JsonResponse
     * @author Ziad
     */
    public function products(Request $request)
    {
        $user = $request->auctionUser;
        $products = $user->products->where('approved',1)->where('active',1);
        $products = ProductResource::collection($products);
        return response()->success($products);
    }

    /**
     * to get user bids
     * @return JsonResponse
     * 
     * @param jwtToken
     * @author Ziad
     */
    public function bids(Request $request)
    {
        $user = $request->auctionUser;
        $products = $user->bids->map(function(Bid $item)
        {
            return $item->product;
        });
        
        $products = $products->where('active',1)->where('approved',1);
        return response()->success(ProductResource::collection($products)->collection);
    }
}
