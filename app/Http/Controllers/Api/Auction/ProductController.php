<?php

namespace App\Http\Controllers\Api\Auction;

use App\Auction\Product as AppProduct;
use App\Auction\ProductImages;
use App\Auction\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Resources\Auction\HomeResource;
use App\Http\Resources\Auction\ProductResource;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Products\Product;
use Illuminate\Http\JsonResponse;
use Validator;

class ProductController extends Controller
{

    private $categoryRepo;

    public function __construct()
    {
        $this->middleware('jwtMiddleware', ['only' => ['index','filter','store' , 'update' , 'destroy']]);
    }

    /**
    * Display Listing Of The Resource
    *@return JsonResponse 
    */
    public function index(Request $request)
    {
        if($request->has('sortBy'))
        {
             $sortBy = $request->sortBy;
            $orderBy = false; 
            if($request->has('orderBy'))
            {
                if($request->orderBy == 'desc')
                {
                    $orderBy = true;
                }
            }
                $products = AppProduct::sort($sortBy,$orderBy);;
        }
        else
        {
            $products = AppProduct::getAllProducts();
        }
        return response()->success(HomeResource::collection($products));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validate = $this->valid($request);
        $productImage = new ProductImages();
        if($validate->fails())
        {
            return response()->json(['errors'=>$validate->errors(),401]);
        }
        $AppProduct              = AppProduct::createFromTitle($request->name);
        $AppProduct->user_id     = $request->auctionUser->id;
        $AppProduct->description = $request->description;
        $AppProduct->min_increment = $request->min_increment;
        $AppProduct->country = $request->country;
        $AppProduct->category_id = $request->category_id;
        $AppProduct->city = $request->city;
        $AppProduct->start_price   = $request->start_price; //TODO use this for something? Buy it now maybe.
        $AppProduct->end_date    = Carbon::now()->addDays($request->duration);
        $AppProduct->address = $request->address == null ? null : $request->address;

        if ($request->hasFile('cover')) {

            $AppProduct->cover = $AppProduct->saveCoverImage($request->file('cover'));
        }
        if ($request->hasFile('cover')) {

            $AppProduct->cover = $AppProduct->saveCoverImage($request->file('cover'));
        }
        
        

        $AppProduct->save();
        if($request->hasFile('src'))
        {
            $images = $request->file('src');
            foreach($images as $src)
            {
                $image =new ProductImages();
                
                $imageName = rand().'.'.$src->getClientOriginalExtension();
                 $src->move(public_path('auction/products/'), $imageName);
                
                $image->src = 'auction/products/'.($imageName);
                $image->product_id = $AppProduct->id;
                $image->save();

            }
        }
    

        // if ($request->hasFile('image')) {

        //     $AppProduct->saveProductImages($request->file('image'));
        // }


        $AppProduct = new ProductResource($AppProduct);

        return response()->success($AppProduct);
    }

    /**
     * check validation 
     * @param Request $request
     * 
     * @return validation
     * 
     * @author Amr Degheidy
     */
    public function valid($request)
    {
        return Validator::make($request->all(),[
            'name'=> 'required',
            'description'=>'required',
            'start_price'=>'required|numeric',
            'min_increment'=>'required|numeric',
            'country'=>'required|string',
            'city'=>'required|string',
            'category_id'=>'required',
            'end_date'=>'required|date',
            'src.*'=>'image'
        ]);
    }
/**
 * delete images from product image for auction
 * @param Request $id
 * @author Amr Degheidy
 * @return Illuminate\Http\Response
 */
    public function deleteImage(Request $request)
    {
       if(!auth()->guard('customers')->user())
       {
           return response()->json(['you are not authorized']);
       };
       
        foreach($request->image as $image)
        
        {
            $productImage = ProductImages::find($image);
            if($productImage == null)
            {
                return response()->success('false');
            }
            
            $productImage->delete();
            $productImage->save();
        }
        return response()->success('true');
    }
/**
 * adding image to current project
 * @param Product $id
 * 
 * @param Request images
 * 
 * @return Illuminate\Http\Response
 * 
 * @author Amr Degheidy
 */
    public function addImage(int $id ,Request $request)
    {
       if(!auth()->guard('customers')->user())
       {
           return response()->json(['you are not authorized']);
       };
       if($request->hasFile('src'))
        {
            $validate = Validator::make($request->all(),[
                'src.*'=>'image'
            ]);
            if( $validate->fails())
            {
                return response()->json(['errors'=>$validate->errors(),401]);
            }
            $images = $request->file('src');
            
            foreach($images as $src)
            {
                $image =new ProductImages();
                
                $imageName = rand().'.'.$src->getClientOriginalExtension();
                 $src->move(public_path('auction/products/'), $imageName);
                
                $image->src = 'auction/products/'.($imageName);
                $image->product_id = $id;
                $image->save();

            }
        }
        return response()->success('true');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $product = AppProduct::find($id);
        $product = new ProductResource($product);
        return response()->success($product);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {   
        $AppProduct = AppProduct::find($id);
        $validate = $this->valid($request);

        if(!$AppProduct->checkValidation())
        {
            return response()->success('You are not authorized to edit your auction because there are bids on your auction.');
        }

        if($validate->fails())
        {
            return response()->json(['errors'=>$validate->errors(),401]);
        }

        if($AppProduct->user_id = $request->auctionUser->id)
        {
            $AppProduct->name = $request->name;
            $AppProduct->slug = str_slug($request->name);
            $AppProduct->user_id     = $request->auctionUser->id;
            $AppProduct->description = $request->description;
            $AppProduct->category_id = $request->category_id;
            
            $AppProduct->min_increment = $request->min_increment;
            $AppProduct->country = $request->country;
            $AppProduct->city = $request->city;
            $AppProduct->start_price   = $request->start_price; //TODO use this for something? Buy it now maybe.
            $AppProduct->end_date    = Carbon::now()->addDays($request->duration);
            $AppProduct->address = $request->address == null ? null : $request->address;
            
            if ($request->hasFile('cover')) {

                $AppProduct->cover = $AppProduct->saveCoverImage($request->file('cover'));
            }

            // if ($request->hasFile('image')) {

            //     $AppProduct->saveProductImages($request->file('image'));
            // }

            $AppProduct->update();
            
            $AppProduct = new ProductResource($AppProduct);

            return response()->success($AppProduct);
        }

        else 
        {
            return response()->json(['success' => 'false','data' => ['message' => 'You Are Not Allowed']]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request,int $id)
    {
        $product = AppProduct::find($id);
        
        if(!$product->checkValidation())
        {
            return response()->success('You are not authorized to delete your auction because there are bids on your auction.');
        }
        

        if($product->user_id = $request->auctionUser->id)
        {
            $product->delete();
            return response()->success([]);
        }
        else 
        {
            return response()->json(['success' => 'false','data' => ['message' => 'You Are Not Allowed']]);
        }
    }

    /*
    * @param name
    * @return Product
    */
   public function search(Request $request)
   {   $name = $request->name;
       $products = \DB::table('auction_products')->where('name','LIKE',"%$name%")->get();
       return $products;
   }

    /**
     * Get Trend Products
     * @return JsonResponse
     */
    public function trend()
    {
        $trend = ProductResource::collection(AppProduct::trend()->get())->collection;
        return response()->success($trend);
    }

    /**
     * Renew Auction
     * @return JsonResponse
     */
    public function renew(int $id)
    {
        $product = AppProduct::find($id);
        if($product->renewAuction())
        {
            return response()->success([]);
        }
        return response()->json(['success'=>'false','data'=>['message'=>'Sorry you reached the max limit of renews']]);
    }

    public function getFilterValues()
    {
        $categories = CategoryRepository::filteredCategories();
        return response()->success($categories);
    }

    public function filter(Request $request)
    {
        $min_price = 0;

        $max_price = 1000000;

        $categories = [];
        
        if($request->has('min_price') && $request->has('max_price'))
        {
            $min_price = $request->min_price;
            $max_price = $request->max_price;
        }

        else if($request->has('min_price'))
        {
            $min_price = $request->min_price;
        }

        else if($request->has('max_price'))
        {
            $max_price = $request->max_price;
        }

        if($request->has('category'))
        {   
            $categories = explode(',',$request->category);
        }

        $products = AppProduct::filter($min_price,$max_price,$categories);
        return response()->success(HomeResource::collection($products));
    }
}
