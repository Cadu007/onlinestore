<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Product\ProductResource;
use App\Shop\Categories\Repositories\CategoryRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepo;

    /**
     * @var Request
     */
    protected $request;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepo
     * @param Request $request
     */
    public function __construct( CategoryRepositoryInterface $categoryRepo, Request $request )
    {
        $this->categoryRepo = $categoryRepo;

        $this->request = $request ;
    }

    /**
     * Get all root Categories
     *
     * @return JsonResponse
     */
    public function index()
    {
        $categories = CategoryResource::collection($this->categoryRepo->rootCategories())->collection;
        $categories = $this->filterRejected($categories);
        return response()->success( $categories );
    }
    /**
     * remove matching element for Category
     * @param Category
     * @return Category
     * 
     */

    public function filterRejected($categories)
    {
        $filtred = $categories->reject(function($item)
        {
            return $item->name == "UnCategorized";
        });

        return $filtred->values()->toArray();
    }
    /**
     * filter category
     * @param $id
     * @return Response
     */

    public function filterValues($id)
    {
        $cat = Category::find($id);
        $catRepo = new CategoryRepository($cat);
        $attrs = $catRepo->listAttributesValues();
        $brands = $catRepo->getBrands();
        $data = [
            'attributes' => $attrs,
            'brands' => $brands
        ];
        return response()->success($data);        
    }
    /**
     * find category
     * @param Category $id
     * @param Request
     * @return Response
     */
    public function filter($id,Request $request)
    {
        $cat = Category::find($id);
        $catRepo = new CategoryRepository($cat);
        $products = $catRepo->filter($request);
        return response()->success($products);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws \App\Shop\Categories\Exceptions\CategoryNotFoundException
     */
    public function show( int $id, Request $request)
    {
        $category = $this->categoryRepo->findCategoryById($id);

        $categoryRepo = new CategoryRepository($category);

        $children = CategoryResource::collection($category->children)->collection;
        $products = ProductResource::collection($categoryRepo->findProducts())->collection;

        $data = new CategoryResource($category);
        $data = $data->toArray($request);

        if($category->parent_id == null)
        {
            $data['children'] = $children ;
            $data['products'] = ProductResource::collection($categoryRepo->getChildrenProducts())->collection;
        }
        else{
            $data['products'] = $products ;
        }
        return response()->success( $data );
    }

    /**
     * @param $id
     * @return JsonResponse
     * @TODO handle exception if category doesn't exist
     * @throws \App\Shop\Categories\Exceptions\CategoryNotFoundException
     */
    public function sort( int $id )
    {
        $category = $this->categoryRepo->findCategoryById($id);

        $categoryRepo = new CategoryRepository($category);

        $categories = ProductResource::collection($categoryRepo->sortProductForCategory($id))->collection;

        return response()->success( $categories );
    }
}
