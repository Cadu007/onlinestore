<?php

namespace App\Http\Controllers\Api\Auth ;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails ;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validation_rules = ['email' => 'required|email'];

        $validator = Validator::make( $request->all(), $validation_rules );

        if( $validator->fails() )
        {
            return response()->fail( $validator->errors()->all() );
        }

        $response = $this->broker()->sendResetLink( $request->only('email') );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkSuccessResponse($request, $response)
            : $this->sendResetLinkFailResponse($request, $response);
    }

    public function sendResetLinkFailResponse()
    {
        return response()->fail(['Couldn\'t send password reset please try to communicate our support']);
    }

    public function sendResetLinkSuccessResponse()
    {
        return response()->success(['Email sent successfully']);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('customers');
    }
}