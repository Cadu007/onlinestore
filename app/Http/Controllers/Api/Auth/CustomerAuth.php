<?php


namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Shop\Phone\Phone;
use App\Services\Authentication\Customer\Login;
use App\Services\Authentication\Customer\Registration;
use App\Shop\Customers\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Propaganistas\LaravelPhone\PhoneNumber;


class CustomerAuth extends Controller
{
    /**
     * validate registration Type
     *
     * @param $type
     * @return bool
     */
    protected function isTypeValid( $type )
    {
        if( ! $type || is_null( $type ) || ! in_array( $type, Customer::getAllowedRegistrationTypes() ) )
        {
            return false ;
        }
        return true;
    }

    /**
     * @param $validation_rules
     * @param $type
     * @return array
     */
    protected function addPasswordValidationIfNormal( $validation_rules, $type )
    {
        if ( $type == Customer::REGISTRATION_TYPES['normal'] )
        {
            $validation_rules['password'] = 'required|min:8';
        }
        return $validation_rules ;
    }
    
    /**
     * @param $type
     * @return array
     */
    protected function buildValidationRules( $type )
    {
        $validation_rules = [
            'name'          => 'required|min:5',
            'email'         => 'required|email:rfc,dns|unique:customers',
            'phone'         => 'required|phone|unique:customers',
            'phone_country' => 'required|required_with:phone',
            'code'          => 'required|min:4',
            'device_id'     => 'required',
        ] ;
        return $this->addPasswordValidationifNormal( $validation_rules, $type );
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isCodeValid( Request $request )
    {
        $phone_country  = $request->get('phone_country');
        $phone          = $request->get('phone');
        $code           = $request->get('code');

        $valid_phone = (string) PhoneNumber::make( $phone, $phone_country );

        $saved_code = Phone::getPhoneCode( $valid_phone );

        return $saved_code == $code ;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function register( Request $request )
    {
        $type = $request->get('type');

        if( ! $this->isTypeValid( $type ) )
        {
            return response()->fail( ['Registration Type is not valid'], 400 );
        }

        $validation_rules = $this->buildValidationRules( $type );

        $validator = Validator::make( $request->all(), $validation_rules );

        if( $validator->fails() )
        {
            return response()->fail( $validator->errors()->all() );
        }
        if( ! $this->isCodeValid( $request ) )
        {
            return response()->fail( ['Code do not match our records'], 400 );
        }
        try{
            $result = Registration::register( $request->all() );

            $user = Auth::guard('customers')->user();

            return response()->success(['token' => $result, 'user' => $user]);
        }
        catch ( \Exception $e )
        {
            return response()->fail( [ $e->getMessage() ] );
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function login( Request $request )
    {
        $type = $request->get('type');

        if( ! $this->isTypeValid( $type ) )
        {
            return response()->fail( ['Registration Type is not valid'], 400 );
        }

        $validation_rules = [ 'email' => 'required|email:rfc,dns'];

        $validation_rules = $this->addPasswordValidationIfNormal( $validation_rules, $type );

        $validator = Validator::make( $request->all(), $validation_rules );

        if( $validator->fails() )
        {
            return response()->fail( $validator->errors()->all() );
        }
        try{
            $token = Login::login( $request->all() );

            $user = Auth::guard('customers')->user();

            return response()->success( [ 'token' => $token, 'user' => $user ] );
        }
        catch ( \Exception $e )
        {
            return response()->fail( [ $e->getMessage() ] );
        }
    }

   
}