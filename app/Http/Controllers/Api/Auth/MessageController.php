<?php


namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Services\Twilio\Twilio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Propaganistas\LaravelPhone\PhoneNumber;

class MessageController extends Controller
{
    /**
     * @return array
     */
    protected function buildValidationRules()
    {
        return [
            'phone'         => 'required|phone',
            'phone_country' => 'required|required_with:phone',
        ];
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function message( Request $request )
    {
        $validation_rules = $this->buildValidationRules();
        $validator = Validator::make( $request->all(), $validation_rules );

        if( $validator->fails() )
        {
            return response()->fail( $validator->errors()->all() );
        }
        try{
            $phone = (string) PhoneNumber::make( $request->get('phone'), $request->get('phone_country') );

            Twilio::createPhoneRecord( $phone );

            return response()->success([]);
        }
        catch ( \Exception $e )
        {
            return response()->fail( $e->getMessage() );
        }
    }
}