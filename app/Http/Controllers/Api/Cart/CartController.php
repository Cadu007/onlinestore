<?php

namespace App\Http\Controllers\Api\Cart;

use App\Shop\Carts\Repositories\CartRepository;
use App\Shop\Carts\Requests\AddToCartRequest;
use App\Shop\Carts\Requests\UpdateCartRequest;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\ProductAttributes\Repositories\ProductAttributeRepositoryInterface;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;

class CartController extends Controller
{
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepo;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepo;

    /**
     * @var CustomerRepositoryInterface
     */
    private $CustomerRepo;

    /**
     * @var Request
     */
    protected $request;

    /**
     * CartController constructor.
     *
     * @param CartRepositoryInterface $cartRepo
     * @param Request $request
     */
    public function __construct(
        CartRepositoryInterface $cartRepo,
        ProductRepositoryInterface $productRepository,
        CourierRepositoryInterface $courierRepository,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        CustomerRepositoryInterface $CustomerRepositoryInterface,
        Request $request
    ) {
        $this->cartRepo = $cartRepo;
        $this->productRepo = $productRepository;
        $this->courierRepo = $courierRepository;
        $this->productAttributeRepo = $productAttributeRepository;
        $this->CustomerRepo = $CustomerRepositoryInterface;

        $this->request = $request ;

        $this->middleware('auth:customers');
    }

    /**
     * Instantiate cart model
     */
    public function instantiateCart()
    {
        $cart = $this->cartRepo->openCart( $this->getCurrentUser() );

        $this->cartRepo = new CartRepository( $cart );
    }


    /**
     * @return Customer
     */
    protected function getCurrentUser(): Customer
    {
        return $this->CustomerRepo->findCustomerById( auth('customers')->id() );
    }

    /**
     * @return mixed
     */
    public function returnResponse()
    {
        $courier = $this->courierRepo->findCourierById(request()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($courier);

        return response()->success( [
            'cartItems' => $this->cartRepo->transformCartItems(),
            'subtotal' => $this->cartRepo->getSubTotal(),
            'tax' => $this->cartRepo->getTax(),
            'shippingFee' => $shippingFee,
            'total' => $this->cartRepo->getTotal(2, $shippingFee)
        ] );
    }

    /**
     * Get Cart
     *
     * @return JsonResponse
     */
    public function index()
    {
        $this->instantiateCart();

        $this->cartRepo->saveCart( $this->getCurrentUser() );

        return $this->returnResponse();
    }

    /**
     * @param AddToCartRequest $request
     * @return JsonResponse
     */
    public function store( AddToCartRequest $request )
    {
        $this->instantiateCart();

        $product = $this->productRepo->findProductById($request->input('product'));

        if ($product->attributes()->count() > 0) {
            $productAttr = $product->attributes()->where('default', 1)->first();

            if (isset($productAttr->sale_price)) {
                $product->price = $productAttr->price;

                if (!is_null($productAttr->sale_price)) {
                    $product->price = $productAttr->sale_price;
                }
            }
        }

        $options = [];

        if ($request->has('productAttribute')) {

            $attr = $this->productAttributeRepo->findProductAttributeById($request->input('productAttribute'));
            $product->price = $attr->price;

            $options['product_attribute_id'] = $request->input('productAttribute');
            $options['combination'] = $attr->attributesValues->toArray();
        }

        $this->cartRepo->addToCart($product, $request->input('quantity'), $options);

        $this->cartRepo->saveCart( $this->getCurrentUser() );

        return $this->returnResponse();
    }

    /**
     * @param UpdateCartRequest $request
     * @param $id
     * @return mixed
     */
    public function update( UpdateCartRequest $request, $id )
    {
        $this->instantiateCart();

        $this->cartRepo->updateQuantityInCart($id, $request->input('quantity'));

        $this->cartRepo->saveCart( $this->getCurrentUser() );

        return $this->returnResponse();
    }

    /**
     * Delete item from Cart
     *
     * @return JsonResponse
     */
    public function delete($id)
    {
        $this->instantiateCart();

        $this->cartRepo->removeToCart($id);

        $this->cartRepo->saveCart( $this->getCurrentUser() );

        return $this->returnResponse();
    }

    /**
     * Clear Cart
     *
     * @return JsonResponse
     */
    public function clear()
    {
        $this->instantiateCart();

        $this->cartRepo->clearCart();

        $this->cartRepo->saveCart( $this->getCurrentUser() );

        return $this->returnResponse();
    }
}
