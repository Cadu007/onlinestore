<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Resources\Product\ProductResource;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    //
    use ProductTransformable, UploadableTrait;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
    
    /**
     * ProductController constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepo = $productRepository;
        $this->middleware(['permission:create-product, guard:employee'], ['only' => ['create', 'store']]);
        $this->middleware(['permission:update-product, guard:employee'], ['only' => ['edit', 'update']]);
        $this->middleware(['permission:delete-product, guard:employee'], ['only' => ['destroy']]);
//        $this->middleware(['permission:view-product, guard:employee'], ['only' => ['index', 'show']]);
    }


    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        $list = $this->productRepo->listProducts();

        if (request()->has('search') && request()->input('search') != '') {
            $list = Product::searchProductByName(request()->input('search'));
        }

        $list = ProductResource::collection($list)->collection;

        return response()->success($list);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function show(int $id,Request $request)
    {
        $product = $this->productRepo->findProductById($id);
        return response()->success([new ProductResource($product)]);
    }

        /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function getProductReview($id)
    {
        $product = Product::find($id);
        $productRepo = new ProductRepository($product);
        return response()->success([$productRepo->getReview()]);
    }


    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function setProductReview($id , Request $request)
    {
        $product = Product::find($id);
        $productRepo = new ProductRepository($product);
        $request['product_id'] = intval($id);
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
        $request['customer_id'] = $user->id;
        $review = $productRepo->createReview($request->all());
        return response()->success([$review]);
    }
    /**
     * find product combination
     * 
     */
    public function findProductCombination($id ,Request $request)
    {
        $data = $this->productRepo->findCombinations($id,$request);
        return response()->success($data);
    }
    /**
     * find product attributes
     */
    public function getProductAttribute($id, Request $request)
    {
        $data = $this->productRepo->getProductAttributeId($id,$request);
        return response()->success($data);
    }
    /**
     * get most popular product
     */
    public function getPopularProduct()
    {
        return ProductResource::collection($this->productRepo->getPopularProducts()->orderBy('rating','DESC')->get())->collection;
    }
    /**
     * get all new products
     */
    public function getNewProduct()
    {
        return ProductResource::collection($this->productRepo->getNewProducts()->orderBy('created_at','DESC')->get())->collection;
    }

}
