<?php

namespace App\Http\Controllers\Api\Review;

use App\Shop\Orders\Order;
use App\Shop\Review\Review;
use Illuminate\Http\Request;
use App\Shop\Products\Product;
use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    /**
     * Add Review To A Product
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function addReview($id , Request $request)
    {
     

        $customer_id = Auth::guard('customers')->user()->id;
        
       
        $product = DB::table('order_product')->select('product_id')->where('product_id','=',$id)->first();
        // dd($product->product_id);
        
        $order = DB::table('order_product')->select('order_id')->where('product_id','=',$product->product_id)->first();
        // dd($order);
        if($product == null || $order== null)
        {
            return response()->json(['success'=>false],401);
        }
        
        
        $order = Order::findOrFail($order->order_id);
        if($order->customer_id != $customer_id)
        {
            return response()->json(['success'=>false],401);
        }
        
        $review = new Review();
        $review->product_id = $product->product_id;
        $review->customer_id = $customer_id;
        $review->rate = $request->rate;
        $review->content = $request->content;
        
        $review->save();
        // dd($customer_id);
        return response()->json(['success' => true],200);
    }  
}
