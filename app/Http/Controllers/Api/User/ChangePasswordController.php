<?php

namespace App\Http\Controllers\Api\User ;

use App\Http\Controllers\Controller;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepo ;

    public function __construct( CustomerRepositoryInterface $customerRepo )
    {
        $this->customerRepo = $customerRepo ;

        $this->middleware('auth:customers');
    }

    /**
     * @return Customer
     */
    protected function getCurrentUser(): Customer
    {
        return $this->customerRepo->findCustomerById( auth('customers')->id() );
    }

    /**
     * route change password
     *
     * @param Request $request
     * @return mixed
     */
    public function changePassword( Request $request )
    {
        $validation_rules = [
            'new_password'      => 'required|min:8',
            'current_password' => 'required|min:4',
        ];

        $validator = Validator::make( $request->all(), $validation_rules );

        if( $validator->fails() )
        {
            return response()->fail( $validator->errors()->all() );
        }

        $current_password = $request->get('current_password');
        $new_password = $request->get('new_password');

        $customer = $this->getCurrentUser() ;

        if (!( Hash::check( $current_password, $customer->password ) ) ) {
            return response()->fail(['Your current password does not matches with the password you provided. Please try again.']);
        }

        if( strcmp( $current_password, $new_password ) == 0 ) {
            return response()->fail( [ "New Password cannot be same as your current password. Please choose a different password." ] );
        }

        //Change Password
        $customer->password = Hash::make( $new_password );
        $customer->save();

        return response()->success( ['Password changed successfully'] );
    }
}