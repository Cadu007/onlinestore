<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Companies\Company;
use App\Shop\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Shop\Companies\Repositories\CompanyRepository;
use App\Http\Resources\Product\ProductResource;

class CompanyController extends Controller
{
    //

    /**
     * @var CompanyRepositoryInterface
     */
    protected $companyRepo;

    public function __construct(CompanyRepositoryInterface $companyRepo)
    {
        $this->companyRepo = $companyRepo;
    }
    /**
     * return company product
     * @param Company $id
     * @return Response
     */
    public function index(int $id)
    {
        $company = $this->companyRepo->findCompanyById($id);
        $companyRepo = new CompanyRepository($company);
        $products = ProductResource::collection($companyRepo->findProducts())->collection->toArray();
        return response()->success($products);;
    }


}
