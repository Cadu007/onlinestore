<?php

namespace App\Http\Controllers\Api\Mall;

use App\Mall\Mall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Mall\MallResource;

class MallController extends Controller
{
    /**
     * return all malls
     */
    public function index()
    {
        $malls = Mall::all();
        return response()->success(MallResource::collection($malls));
        
    }
    /**
     * show mall
     * @param Mall $id
     * @return Response with data resource
     * 
     */
    public function show($id)
    {
        $mall = Mall::findOrFail($id);
        return response()->success(new MallResource($mall));
    }

}
