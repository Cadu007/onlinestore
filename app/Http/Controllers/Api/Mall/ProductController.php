<?php

namespace App\Http\Controllers\Api\Mall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Mall\ProductResource;
use App\Mall\Product;
use App\Mall\Store;

class ProductController extends Controller
{
    //
    /**
     * return product for specific store in specif mall
     * @param Mall $mallId
     * @param Store $storeId
     */
    public function index($mallId,$storeId)
    {
        $store = Store::find($storeId);
        $products = $store->products;


        return response()->success(ProductResource::collection($products)->collection);
    }
    /**
     * return product details
     * @param Mall $mallId
     * @param Store $storeId
     * @param Product $productId
     * @return Product data
     */
    public function show($mallId,$storeId,$productId)
    {
        $product = Product::find($productId);
        return response()->success($product);
    }
}
