<?php

namespace App\Http\Controllers\Api\Mall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Mall\StoreResource;
use App\Mall\Mall;

class StoreController extends Controller
{
    //
    /**
     * all store for specific mall
     * @param Mall $mallId
     * @return response with data resource
     */
    public function index($mallId)
    {   
        $mall = Mall::find($mallId);
        $stores = $mall->stores()->where('approved',1);
        return response()->success(StoreResource::collection($stores->get())->collection);
    }
}
