<?php

namespace App\Http\Controllers\Admin\Addresses;

use App\Shop\Addresses\Address;
use App\Shop\Addresses\Repositories\AddressRepository;
use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Requests\CreateAddressRequest;
use App\Shop\Addresses\Requests\UpdateAddressRequest;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Cities\City;
use App\Shop\Cities\Repositories\Interfaces\CityRepositoryInterface;
use App\Shop\Countries\Country;
use App\Shop\Countries\Repositories\CountryRepository;
use App\Shop\Countries\Repositories\Interfaces\CountryRepositoryInterface;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Provinces\Repositories\Interfaces\ProvinceRepositoryInterface;
use Illuminate\Http\Request;
use App\Shop\Companies\Company;

class AddressController extends Controller
{
    use AddressTransformable;

    private $addressRepo;
    private $customerRepo;
    private $countryRepo;
    private $provinceRepo;
    private $cityRepo;
    private $companyRepo;

    public function __construct(
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        CountryRepositoryInterface $countryRepository,
        ProvinceRepositoryInterface $provinceRepository,
        CityRepositoryInterface $cityRepository,
        CompanyRepositoryInterface $companyRepo
    ) {
        $this->addressRepo = $addressRepository;
        $this->customerRepo = $customerRepository;
        $this->countryRepo = $countryRepository;
        $this->provinceRepo = $provinceRepository;
        $this->cityRepo = $cityRepository;
        $this->companyRepo = $companyRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $list = $this->addressRepo->listAddress('created_at', 'desc');

        if ($request->has('q')) {
            $list = $this->addressRepo->searchAddress($request->input('q'));
        }

        $addresses = $list->map(function (Address $address) {
            return $this->transformAddress($address);
        })->all();

        return view('admin.addresses.list', ['addresses' => $this->addressRepo->paginateArrayResults($addresses)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = $this->countryRepo->listCountries();
        $country = $this->countryRepo->findCountryById(1);

        $customers = $this->customerRepo->listCustomers();

        return view('admin.addresses.create', [
            'customers' => $customers,
            'countries' => $countries,
            'provinces' => $country->provinces,
            'cities' => City::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAddressRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAddressRequest $request)
    {
        $request->user_type = 'customer';
        $this->addressRepo->createAddress($request->except('_token', '_method'));

        $request->session()->flash('message', 'Creation successful');
        return redirect()->route('admin.addresses.index');
    }

    public function createCompanyAddress()
    {
        $countries = $this->countryRepo->listCountries();
        $country = $this->countryRepo->findCountryById(1);

       

        // dd($companies);
        $companies = auth()->guard('company')->user();
        
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        return view('company.addresses.create-address',[
            'countries' =>$countries,
            'provinces'=> $country->provinces,
            'cities'=>City::all(),
            'company'=>$companies,
            'breadcumbs'=>$breadcumb

        ]);
    }

    /**
     * Add address to company
     * @return View
     * 
     * @author Amr Degheidy
     */
    public function storeCompanyAddress(CreateAddressRequest $request, int $id)
    {
        $company = auth()->guard('company')->user() ;
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        if( $company->id !== $id )
        {
            $request->session()->flash('message' ,'Update Failed');    
            return view('company.profile.view-profile',['company'=>$company,
            'breadcumbs'=>$breadcumb]);
        }
        $companyAddress = new Address();
        $companyAddress->alias = $request->alias;
        $companyAddress->address_1 = $request->address_1;
        $companyAddress->address_1 = $request->address_1;
        $companyAddress->address_2 = $request->address_2;
        $companyAddress->country_id = $request->country_id;
        $companyAddress->zip = $request->zip;
        $companyAddress->user_type = 'company';
        $companyAddress->user_id = $id;

        $companyAddress->save();
        
        // $this->addressRepo->createAddress($request->except('_token', '_method'));
        $request->session()->flash('message','Creation Successful');
        
        return view('company.addresses.view-addresses',['company'=>$company,'breadcumbs'=>$breadcumb]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return view('admin.addresses.show', ['address' => $this->addressRepo->findAddressById($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $countries = $this->countryRepo->listCountries();

        $country = $countries->filter(function ($country) {
            return $country == env('SHOP_COUNTRY_ID', '1');
        })->first();

        $countryRepo = new CountryRepository(new Country);
        if (!empty($country)) {
            $countryRepo = new CountryRepository($country);
        }

        $address = $this->addressRepo->findAddressById($id);
        $addressRepo = new AddressRepository($address);
        $customer = $addressRepo->findCustomer();

        return view('admin.addresses.edit', [
            'address' => $address,
            'countries' => $countries,
            'countryId' => $address->country->id,
            'provinces' => $countryRepo->findProvinces(),
            'provinceId' => $address->province->id,
            'cities' => $this->cityRepo->listCities(),
            'cityId' => $address->city_id,
            'customers' => $this->customerRepo->listCustomers(),
            'customerId' => $customer->id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAddressRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAddressRequest $request, $id)
    {
        $address = $this->addressRepo->findAddressById($id);

        $update = new AddressRepository($address);
        $update->updateAddress($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.addresses.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = $this->addressRepo->findAddressById($id);
        $delete = new AddressRepository($address);
        $delete->deleteAddress();

        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('admin.addresses.index');
    }
}
