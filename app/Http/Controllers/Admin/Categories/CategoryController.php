<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Shop\Attributes\Attribute;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Categories\Requests\CreateCategoryRequest;
use App\Shop\Categories\Requests\UpdateCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryRepo->rootCategories('created_at', 'desc');
        $categories->map(function ($item)
        {
            $catRepo = new CategoryRepository($item);
            $attributes = $catRepo->listAttributes()->implode('name',' - ');
            $item->attributes = $attributes;
            return $item;
        });
        return view('admin.categories.list', [
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create', [
            'categories' => $this->categoryRepo->rootCategories('name','asc'),
            'attributes' => Attribute::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $this->categoryRepo->createCategory($request->except('_token', '_method'));

        return redirect()->route('admin.categories.index')->with('message', 'Category created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->categoryRepo->findCategoryById($id);
        $list = $category->children;

        $list->map(function($item)
        {
            $cat = Category::find($item->id);
            $catRepo = new CategoryRepository($cat);
            $item->attributes = $catRepo->listAttributes()->map(function ($item)
            {
                return $item->name;
            });
            $item->attributes = implode(" - ",$item->attributes->toArray());
            return $item;
        });

        $cat = new CategoryRepository($category);

        return view('admin.categories.show', [
            'category' => $category,
            'categories' => $category->children,
            'products' => $cat->findProducts()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepo->findCategoryById($id);
        $categoryRepo = new CategoryRepository($category);
        $selectedAttributes = $categoryRepo->listAttributes()->pluck('id');
        return view('admin.categories.edit', [
            'categories' => $this->categoryRepo->rootCategories('name', 'asc', $id),
            'category' => $this->categoryRepo->findCategoryById($id),
            'attributes' => Attribute::all(),
            'categoryAttributes' => $selectedAttributes->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCategoryRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = $this->categoryRepo->findCategoryById($id);
        $update = new CategoryRepository($category);
        $update->updateCategory($request->except('_token', '_method'));

        if ($request->has('Attr')) {
            $update->syncAttributes($request->input('Attr'));
        } else {
            $update->detachAttributes();
        }

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.categories.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $category = $this->categoryRepo->findCategoryById($id);
        $catRepo = new CategoryRepository($category);
        $catRepo->detachAttributes();
        $category->delete();

        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('admin.categories.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage(Request $request)
    {
        $categoryRepo = new CategoryRepository(Category::find($request->get('category')));
        $categoryRepo->deleteFile($request->only('category'));
        request()->session()->flash('message', 'Image delete successful');
        return redirect()->route('admin.categories.edit', $request->input('category'));
    }
}
