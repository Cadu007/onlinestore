<?php

namespace App\Http\Controllers\Admin\Review;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Review\Review;

class ReviewController extends Controller
{
    /**
     * accept reviews for product auctino
     * @return View
     * @author Amr Degheidy
     */
    public function accept()
    {
        $reviews = Review::all()->where('approved',0);
        return view('admin.reviews.accept',
    [
        'reviews'=>$reviews
    ]);
    }
    /**
     * approve  product review
     * @param Review $id
     * @author Amr Degheidy
     * 
     */

    public function approve($id)
    {
        $review = Review::find($id);
        $review->approved = 1;
        $review->save();
        session()->flash('message','Review approved successfully');
        return redirect()->back();

    }
}
