<?php

namespace App\Http\Controllers\Admin\Customers;

use File;
use Validator;
use Illuminate\Http\Request;
use App\Shop\Customers\Customer;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Shop\Customers\Requests\CreateCustomerRequest;
use App\Shop\Customers\Requests\UpdateCustomerRequest;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Transformations\CustomerTransformable;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;

class CustomerController extends Controller
{
    use CustomerTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepo = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->customerRepo->listCustomers('created_at', 'desc');

        if (request()->has('q')) {
            $list = $this->customerRepo->searchCustomer(request()->input('q'));
        }

        $customers = $list->map(function (Customer $customer) {
            return $this->transformCustomer($customer);
        })->all();


        return view('admin.customers.list', [
            'customers' => $this->customerRepo->paginateArrayResults($customers)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $this->customerRepo->createCustomer($request->except('_token', '_method'));

        return redirect()->route('admin.customers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $customer = $this->customerRepo->findCustomerById($id);
        
        return view('admin.customers.show', [
            'customer' => $customer,
            'addresses' => $customer->addresses
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.customers.edit', ['customer' => $this->customerRepo->findCustomerById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
        $customer = $this->customerRepo->findCustomerById($id);

        $update = new CustomerRepository($customer);
        $data = $request->except('_method', '_token', 'password');

        if ($request->has('password')) {
            $data['password'] = bcrypt($request->input('password'));
        }

        $update->updateCustomer($data);

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.customers.edit', $id);
    }

     /**
     * update data if changes and validate it
     * 
     * @param $request and token
     * @author Amr Degheidy
     * @return Customer
     */
    public function updateUser(Request $request)
    {
        
      
        $user = Auth::guard('customers')->user();

        $validate = Validator::make($request->all(),[
            'name'          => 'required|min:5',
            'email'         => 'required|email:rfc,dns|unique:customers,email,'.$user->id,
        ]);
        if($validate->fails())
        {
            return response()->fail($validate->errors()->all());
        }
            $customer = Customer::findOrFail($user->id);
            if($customer->name != $request->name)
                {
                    $customer->name = $request->name;
                    
                }
                if($user->email != $request->email)
                {
                    $customer->email = $request->email;
                }
               
            if(isset($request->avatar))
            {
                if($customer->avatar != null)
                {
                    File::delete($customer->avatar);
                }
                $avatar = $request->file('avatar');
                $avatar_name = rand().'.'.$avatar->getClientOriginalExtension();
                $avatar->move(public_path('avatars'),$avatar_name);
                $customer->avatar = 'avatars/'.$avatar_name;
                
                $customer->avatar = asset($customer->avatar);
            }
            $customer->update();
            
        return response()->json($customer ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $customer = $this->customerRepo->findCustomerById($id);

        $customerRepo = new CustomerRepository($customer);
        $customerRepo->deleteCustomer();

        return redirect()->route('admin.customers.index')->with('message', 'Delete successful');
    }
}
