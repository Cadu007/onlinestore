<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\QuestionAnswer;


class QuestionAnswerController extends Controller
{
    /**
     * all questions and answers for online store
     * @return QuestionAnswer
     * @author Amr Degheidy
     */
    public function index()
    {
        $qa = QuestionAnswer::all();
        return view('admin.qa.index',[
            'qa'=> $qa
        ]);
    }
    /**
     * create question and answer
     * @return view
     * @author Amr Degheidy
     */
    public function create()
    {
        return view('admin.qa.create');
    }
    /**
     * store question and answer
     * @param Request
     * @return view
     * @author Amr Degheidy
     */
    public function store(Request $request)
    {
        $qa =QuestionAnswer::create($request->except('_Token'));
        return redirect()->back()->withMessage('Created Successfully');
    }
    /**
     * eidt question and answer
     * @param QuestionAnswer $id
     * @return view
     * @author Amr Degheidy
     */
    public function edit($id)
    {
        $qa = QuestionAnswer::findOrFail($id);
        return view('admin.qa.edit',[
            'qa'=>$qa
        ]);
    }
    /**
     * update question and answer
     * @param Request
     * @param QuestionAnswer $id
     * @return view
     * @author Amr Degheidy
     */
    public function update(Request $request, $id)
    {
        $qa = QuestionAnswer::findOrFail($id);
        $qa->question = $request->question;
        $qa->answer = $request->answer;
        $qa->update();
        return redirect('questionAnswer');
    }
    /**
     * delete Question and answer
     * @param QuestionAnswer $id
     * @return Route
     * @author Amr Degheidy
     */
    public function destroy($id)
    {
        $qa = QuestionAnswer::findOrFail($id);
        $qa->delete();
        return redirect('questionAnswer');
    }
    /**
     * return all quesitons and answers 
     * @return Response
     * @author Amr Degheidy
     */
    public function allQA()
    {
        return response()->json(QuestionAnswer::all(),200);
    }
    
}
