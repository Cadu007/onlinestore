<?php

namespace App\Http\Controllers\Admin\Auction;

use App\Auction\Bid;
use App\Auction\Product;
use App\Auction\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuctionController extends Controller
{
    /**
     * return all products in auction module
     * @author Amr Degheidy
     * @return view
     */
    public function index()
    {
        $bids = Bid::with('user')->with('product')->get();

        return view('admin.auction.index', [
            'bids'=>$bids
        ]);
    }
    /**
     * return all approved auction from admin panal
     * @author Amr Degheidy
     * @return view
     */
    public function approvedAuction()
    {
       $products =  Product::all()->where('approved',1);
       return view('admin.auction.approved',[
           'products' =>$products
       ]);
    }
    /**
     * return all  unapproved auction
     * @author Amr Degheidy
     * @return view
     */
    public function unapprovedAuction()
    {
        $products =  Product::all()->where('approved',0);
       return view('admin.auction.unapproved',[
           'products' =>$products
       ]);
    }

    /**
     * approve auction
     * @param product $id
     * @author Amr Degheidy
     * @return View
     */
    public function approve($id)
    {
        
        $product = Product::findOrFail($id);
        $product->approved = 1;
        $product->save();
        return redirect()->back();
    }
}
