<?php

namespace App\Http\Controllers\Admin\Company;

use Illuminate\Http\Request;
use App\Shop\Companies\Company;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Shop\Companies\Requests\LoginRequest;
use App\Shop\Companies\Requests\CreateCompanyRequest;
use App\Shop\Companies\Requests\UpdateCompanyRequest;
use App\Shop\Companies\Repositories\CompanyRepository;
use App\Shop\Countries\Repositories\CountryRepository;
use App\Shop\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Shop\Countries\Repositories\Interfaces\CountryRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * @var CompanyRepository
     */
    private $companyRepo ;

    /**
     * @var CountryRepository
     */
    private $countryRepo;

    /**
     * CompanyController Constructor
     * @param CompanyRepositoryInterface
     */
    public function __construct(
        CompanyRepositoryInterface $companyRepo,
        CountryRepositoryInterface $countryRepo
    ) {
        $this->companyRepo = $companyRepo;
        $this->countryRepo = $countryRepo;

        $this->middleware('guest')->only('create');
    }

    /**
     * Create company
     * @return View with countries
     * 
     * @author Amr Degheidy
     */
    public function create()
    {
        $countries = $this->countryRepo->listCountries();

        return view('auth.register', compact('countries') );
    }

    /**
     * @param Request $request
     * @param $name
     * @return bool
     */
    protected function validatefileExists( Request $request, $name )
    {
        return $request->hasFile( $name ) && $request->file( $name ) instanceof UploadedFile ;
    }

    /**
     * Accept companies
     * @author Amr Dehgiedy
     * @return view
     */
    public function acceptCompany()
    {
        $unAuthorizedCompanies = $this->companyRepo->listCompanies()->where('authorized',0);
        return view('admin.company.accept-company',
        [
            'companies'=> $unAuthorizedCompanies,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @author Amr Degheidy
     */
    public function approveCompany($id)
    {
        $company= Company::findOrFail($id);
        $company->authorized = 1;
        $company->save();
        return redirect()->back();
    }

    /**
     * unauthorized the company
     * @author Amr Degheidy
     * @return view
     */
    public function unauthorizedCompany()
    {
        $authorizedCompanies = $this->companyRepo->listCompanies()->where('authorized',1);

        return view('admin.company.deactivate-company',
        [
            'companies'=> $authorizedCompanies
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @author Amr Degheidy
     * 
     */
    public function unApproveCompany($id)
    {
        $company = Company::findOrFail($id);
        $company->authorized = 0;
        $company->save();
        return redirect()->back();
    }

    /**
     * Store new company in storage
     * 
     * @param CreateCompanyRequest
     * @return \Illuminate\Http\Request
     * 
     */
    public function store(CreateCompanyRequest $request)
    {
        $this->companyRepo->createCompany($request->except('_method','_token'));
        return redirect()->route('admin.company.index');
    }

    /**
     * Display company
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     * @author Amr Degheidy
     */

    public function show()
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        return view('company.profile.view-profile',
        [
            'company'=>auth('company')->user(),
            'breadcumbs' => $breadcumb,
           
        ]);
    }

    /**
     * show the edit form
     * @param int $id
     * @return \Illuminate\Http\Response
     * 
     * @author Amr Degheidy
     */

    public function edit()
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        
        return view('company.profile.edit-profile',
    ['company'=> auth('company')->user(),
        'breadcumbs'=>$breadcumb,
    ]);

    }

    /** 
     * Update company  data
     * @param UpdateCompanyRequest
     * @param int $id
     * @return \Illuminate\Http\Response
     * @author Amr Degheidy
     */
    public function update(UpdateCompanyRequest $request , int $id)
    {
        $company = auth()->guard('company')->user() ;

        if( $company->id !== $id )
        {
            $request->session()->flash('message' ,'Update Failed');    
            return redirect('/address/view-addresses');
        }
        $company = $this->companyRepo->findCompanyById($id);
        $update = new CompanyRepository($company);

        $data = $request->except('_token','_method');
        $update->updateCompany($data);
        $request->session()->flash('message' ,'Update Success');
       
        return redirect('company');
    }

/**
 * change password for company
 * @return View
 * @author Amr Degheidy
 */
    public function changePassword()
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        
        return view('company.profile.change-password',
    ['company'=> auth('company')->user(),
        'breadcumbs'=>$breadcumb,
    ]);

    }
    /**
     * update the password
     * @param request password and confirm password
     * @param Company $id
     * @author Amr Degheidy
     */

    public function updatePassword(Request $request , $id)
    {
        $company = auth()->guard('company')->user();
        $comp = Company::findOrFail($id);
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        if($company->id != $id)
        {
            $request->session()->flash('message','change Password Failed');
            return view('company.profile.view-profile',['company'=> auth('company')->user(),
            'breadcumbs'=>$breadcumb,
        ]);
        }
        $validator = Validator::make($request->all(),[
            'password' => 'min:6'
        ]);
        if(! $validator->fails() && ($request->password === $request->confirm_password))
            {
                $comp->update(['password'=>Hash::make($request->password)]);
            }
            return redirect('company');
    }


/**
 * show company addresses
 * @return View
 * @author Amr Degheidy
 */
     public function showAddresses()
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        return view('company.addresses.view-addresses',
        [
            'company'=>auth('company')->user(),
            'breadcumbs' => $breadcumb,
           
        ]);
    }

    

      /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @author Amr Degheidy
     */
    public function destroy($id)
    {
        $company = $this->companyRepo->findCompanyById($id);

        $companyRepo = new CompanyRepository($company);
        $companyRepo->deleteCompany();

        return redirect()->route('admin.company.index')->with('message', 'Delete successful');
    }
}
