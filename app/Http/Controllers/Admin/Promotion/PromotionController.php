<?php

namespace App\Http\Controllers\Admin\Promotion;

use App\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use File;
class PromotionController extends Controller
{
    /**
     * edit promotions
     * @author Amr Degheidy
     * @return view
     */
    public function editPromotion()
    {   
        $promotion = Promotion::all();

        return view('admin.banner.edit-promotion',[
            'promotion'=>$promotion
       ]);
    }

    /**
     * update promotion
     * 
     * @param Request
     * @author Amr Degheidy
     * @return redirect
     */

    public function updatePromotion(Request $request)
    {
        
        $promotion = Promotion::first();
        if(isset($request->image_1))
        {
            File::delete($promotion->image_1);
        $image_1 = $request->file('image_1');
        $image_1name = rand().'.'.$image_1->getClientOriginalExtension();
        $image_1->move(public_path('promotions'),$image_1name);

            $promotion->image_1 = 'promotions/'.$image_1name;
        }
        if(isset($request->image_2))
        {
            File::delete($promotion->image_2);
            $image_2 = $request->file('image_2');
            $image_2name = rand().'.'.$image_2->getClientOriginalExtension();
            $image_2->move(public_path('promotions'),$image_2name);
           
    
            $promotion->image_2 = 'promotions/'.$image_2name;
        }
        $promotion->update();
        return redirect()->back();
    }
    /***
     * Promotions 
     * 
     * @author Amr Deghiedy
     */

    public function index()
    {
        $promotion = Promotion::first();
        $promotion->image_1 = asset($promotion->image_1);
        $promotion->image_2 = asset($promotion->image_2);
        return response()->json($promotion,200);
    }
   
}
