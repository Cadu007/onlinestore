<?php

namespace App\Http\Controllers\Admin\Mall;

use App\Mall\Mall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Mall\MallResource;
use Validator;
class MallController extends Controller
{

    public function __construct()
    {
        $this->middleware('employee');
    }
    /**
     * all malls
     * @return Collection of Malls
     * @author Amr Degheidy
     */
    public function index()
    {
        $malls = Mall::all();
        return MallResource::collection($malls);
    }
    /**
     * create mall
     * @return View
     * @author Amr Dehgeidy
     */
    public function create()
    {
        return view('mall.view');
    }
    /**
     * sotre mall
     * @param Request
     * @author Amr Degheidy
     * @return View with Data
     */
    public function store(Request $request)
    {
        $validate  = $this->validate($request);
        if($validate->fails())
        {
            session()->flash('message','Create Mall Has Fail.');
            return redirect()->back()->withErrors($validate);
        }
        $mall = Mall::create($request);
        $mall->save();
        session()->flash('message','Mall Has Created Successfully');
        return redirect()->back();
    }
    /**
     * show specific mall
     * @param Mall $id
     * @return View with Mall Data
     * @author Amr Degheidy
     * 
     */
    public function show($id)
    {
        $mall = Mall::findOrFail($id);
        return view('mall.show',compact($mall));
    }

/**
 * edit Mall
 * @@return View
 * @author Amr Deghiedy
 */
    public function edit()
    {
        $malls = Mall::all();
        return view('mall.edit',compact($malls));
    }
    /**
     * update mall
     * @param Request
     * @param Mall $id
     * @author Amr Degheidy
     * @return View with update Data
     */
    public function update(Request $request , int $id)
    {
        $validate = $this->validate($request);
        if($validate->fails())
        {
            session()->flash('message','Update Has Fail.');
            return redirect()->back()->withErrors($validate);
        }
        $mall = Mall::findOrFail($id);
        $mall->update($request)->save();
        session()->flash('message','Mall Updated Successfully');
        return redirect()->back();


    }
    /**
     * 
     * delete mall
     * @param Mall $id
     * @return View
     * @author Amr Deghiedy
     */

    public function destroy($id)
    {
        $mall = Mall::findOrFail($id);
        $mall->destroy();
        session()->flash('message','Mall Deleted Successfully');
        return redirect()->back();
    }

/**
 * Validate Data
 * @return Validate Response
 * @author Amr Degheidy
 */
    public function validate()
    {
        return Validator::make(
        [
            'name'=> 'unique|required',
            'location'=>'required',
            'open_time'=>'time|required',
            'close_time'=>'time|required',
            'map'=>'image|required',
            'cover'=>'image|required',
            'country_id'=>'required',
            'city'=>'required'
        ]);
    }
}
