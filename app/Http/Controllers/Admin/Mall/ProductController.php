<?php

namespace App\Http\Controllers\Admin\Mall;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MallProduct;

class ProductController extends Controller
{
    /**
     * return all product for currant store
     * @return Product
     * @author Amr Degheidy
     */
    public function index()
    {
        $store = $this->auth('store');
        return $store->products();

    }
    /**
     * return view to create product
     * @return View
     * @author Amr Degheidy
     */
    public function create()
    {
        return view('create product');
    }
    /**
     * store product for stor 
     * @param Request
     * @author Amr Degheidy
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name'=>'required',
            'cover'=>'required|image',
            'price  '=>'required',
            'description'=>'required',
            
        ]);
        $request->store_id = $this->auth('store');
        if($validate->fails())
        {
            session()->flash('message','Cannot Create Product');
            return redirect()->back()->withErrors($validate)->withInput();
        }
        session()->flash('message','Product Created Successfully');
        return redirect()->route('index');
    }
    /**
     * edit product for store
     * @param Product $id
     * @return View with data for product
     * @author Amr Degheidy
     */
    public function edit($id)
    {
        $product = MallProduct::findOrFail($id);
        return view('edit view',compact($product));

    }
    /**
     * update product
     * @param Request
     * @return Route with data
     * @author Amr Degheidy
     */
    public function update(Request $request)
    {
        $validate = $request->validate([
            'name'=>'required',
            'cover'=>'required|image',
            'price'=>'required',
            'description'=>'required'
        ]);
        if($validate->fails())
        {
            session()->flash('message','Product Cannot Updated ');
            return redirect()->back()->withErrors($validate)->withInput();
        }
        session()->flash('message','Product Created Successfully');
        return redirect()->route(' index route');
    }
    /**
     * show product
     * @param Product $id
     * @return View
     * @author Amr Degheidy
     */
    public function show($id)
    {
        $product = MallProduct::findOrFail($id);
        return view('show view', compact($product));
    }

    /**
     * delete product
     * @param Product $id
     * @return Route
     * @author Amr Degheidy
     */
    public function destroy($id)
    {
        $product = MallProduct::findOrFail($id);
        $product->destroy();
        session()->flash('message','Product Deleted Successfully');
        return redirect()->route('index route');
    }
}
