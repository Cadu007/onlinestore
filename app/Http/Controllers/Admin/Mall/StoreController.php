<?php

namespace App\Http\Controllers\Admin\Mall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mall\Store;
use App\Shop\Categorie;

class StoreController extends Controller
{
    //
/**
 * Create stor
 * @return View
 * @author Amr Degheidy
 */
    public function create()
    {
        return view('store.create');
    }
    /**
     * Store a store
     * @param Request
     * @return Route
     * @author Amr Degheidy
     */
    public function store(Request $request)
    { 
        $validate = $this->validate($request);
        if($validate->fails())
        {
            return redirect()->back()->withInput(Input::except('password','password_confirmation'));
        }
        $request->password = Hash::make($request->password);
        $store = Store::create($request);
        return redirect()->route('store.panal');

        
    }
/**
 * show Store for mall
 * @param Mall $id
 * @param Store $id
 * @author Amr Degheidy
 */
    public function show($mallId,$storeId)
    {
        $store = Store::find($storeId);
        $data = [
            'id' => $store->id,
            'name' => $store->name,
            'email'=>$store->email,
            'cover' => asset("/storage/$store->cover"),
            'mobile' => $store->mobile,
            'open_time' => $store->open_time,
            'close_time' => $store->close_time,
            'category' => $store->categories,
            'location' => $store->location,
            'approved' => $store->approved
        ];

        return $data;

    }
    /**
     * edit store info
     * @return View
     * @author Amr Degheidy
     */
    public function edit()
    {
        return view('store.edit');
    }
    /**
     * update store info
     * @param Request
     * @param Store $id
     * @author Amr Degheidy
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request);
        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        session()->flash('message','updated completed successfully');
        return redirect()->route('index route');
    }
    /**
     * delete store
     * @param $id
     * @return View
     * @author Amr Degheidy
     */
    public function destroy($id)
    {
        $store = Store::findOrFail($id);
        $store->destroy();
        session()->flash('message','Store Deleted Successfully');
        return redirect()->back();
    }
    /**
     * change password
     * @param Request
     * @author Amr Degheidy
     */
    public function changPassword(Request $request)
    {
        $validate = $request->validate([
            'password'=>'required|min:8',
            'password_confirmation'=>'required|same:password'
        ]);
        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate);
        }
        session()->flash('message','Password Changed Successfully');
        return redirect()->route('index route');
    }

    /**
     * Validate data
     * @return Valid Response
     * @author Amr Degheidy
     */
    public function validate()
    {
        return Validator::make([
            'name'=>'required|unique:mall_stores,name'.$this->id,
            'email'=>'required|unique:mall_stores,email'.$this->id,
            'password'=>'required|min:8',
            'password_confirmation'=>'required|same:password',
            'cover'=>'required|image',
            'mobile'=>'required',
            'open_time'=>'time|required',
            'close_time'=>'time|required|after:open_time',
            'category'=>'required',
            'location'=>'required'
        ]);
    }
}
