<?php

namespace App\Http\Controllers\Admin\Banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Shop\Companies\Company;

class BannerController extends Controller
{


    /**
     * Create banner
     * @author Amr Degheidy
     * @return view
     */
    public function createBanner()
    {
        $companies = Company::all();
        return view('admin.banner.create-banner',
    [
        'companies'=>$companies
    ]);
    }


    /**
     * 
     * store banner
     * 
     * @param $request
     * @author Amr Degheidy
     * @return Illuminate\Http\Response
     */
    public function addBanner(Request $request)
    {
        $this->validate($request,[
            'banner'=>'required|image',
            'company_id' => 'required',
            'start_date'=> 'required|date|before:end_date',
            'end_date'=>'required|date|after:start_date'
        ]);
        $banner = $request->file('banner');
        $bannername = rand().'.'.$banner->getClientOriginalExtension();
        $banner->move(public_path('banners'),$bannername);
       
        $newbanner = new Banner;
        $newbanner->banner = "banners/".$bannername;
        $newbanner->start_date = $request->start_date;
        $newbanner->end_date = $request->end_date;
        $newbanner->company_id = $request->company_id;
        $newbanner->save();
        
        return redirect()->back()->with('message','banner added successfully');
    }

    /**
     * retrieve all banners
     * @author Amr Degheidy
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->getBanners();
        // $banners['banner'] = asset($banners['banner']);
        return response()->json($banners,200);
    }
/**
 * return banner with full path
 * @author Amr Degheidy
 * @return Illuminate\Collection
 */
    public function getBanners()
{
    $banners = Banner::all();
  
    foreach ($banners as $banner)
    {
        $banner->banner = asset($banner->banner);
    }
    return $banners;
}
    
}
