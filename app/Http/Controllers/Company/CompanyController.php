<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    //
    /**
     * Get All Companies
     * @return view
     */
    public function index()
    {        
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        populate_breadcumb($breadcumb);
        return view('company.dashboard', [ 'company' => auth('company')->user() , 'breadcumbs' => $breadcumb]);    
    }

}
