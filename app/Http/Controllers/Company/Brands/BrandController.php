<?php

namespace App\Http\Controllers\Company\Brands;

use App\Http\Controllers\Controller;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Brands\Requests\CreateBrandRequest;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Products\Product;
use App\Shop\Companies\Company;
use App\Shop\Brands\Requests\UpdateBrandRequest;

class BrandController extends Controller
{

    use ProductTransformable;
    /**
     * @var BrandRepositoryInterface
     */
    private $brandRepo;

    private $productRepo;
    /**
     * BrandController constructor.
     *
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository,ProductRepositoryInterface $productRepository)
    {
        $this->brandRepo = $brandRepository;
        $this->productRepo = $productRepository;
    }

    /**
     * Get All Brands
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $company = Company::find(auth('company')->user()->id);
        $data = $company->brands()->get()->all();

        $data = $this->brandRepo->paginateArrayResults($data);


        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];

        return view('company.brands.list', ['brands' => $data , 'company' => auth('company')->user(), 'breadcumbs' => $breadcumb]);
    }

    /**
     * Get Brand Create View
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        return view('company.brands.create', ['company' => auth('company')->user() , 'breadcumbs' => $breadcumb]);
    }

    /**
     * Create Brand
     * @param CreateBrandRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateBrandRequest $request)
    {
        $request['company_id'] = auth('company')->user()->id;
        $this->brandRepo->createBrand($request->all());
        return redirect()->route('company.brands.index')->with('message', 'Create brand successful!');
    }

    /**
     * Get Edit Brand View
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        return view('company.brands.edit', ['brand' => $this->brandRepo->findBrandById($id) , 'company' => auth('company')->user() , 'breadcumbs' => $breadcumb]);
    }

    /**
     * Update Brand 
     * @param UpdateBrandRequest $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Shop\Brands\Exceptions\UpdateBrandErrorException
     */
    public function update(UpdateBrandRequest $request, $id)
    {
        $brand = $this->brandRepo->findBrandById($id);
        $request['company_id'] = auth('company')->user()->id;
        $brandRepo = new BrandRepository($brand);
        $brandRepo->updateBrand($request->all());

        return redirect()->route('company.brands.edit', $id)->with('message', 'Update successful!');
    }

    /**
     * Show The Products that are colerated to a certin brand
     * @param int $id
     * @return View
     */
    public function show($id)
    {
        $brand = $this->brandRepo->findBrandById($id);
        $list = $brand->products;

        $products = $list->map(function (Product $item) {
            return $this->transformProduct($item);
        })->all();

        $breadcumb = [
            ["name" => "Dashboard", "url" => route("company.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("company.dashboard"), "icon" => "fa fa-home"],
        ];
        return view('company.products.list',['products' => $this->productRepo->paginateArrayResults($products, 25) , 'company' => auth('company')->user() , 'breadcumbs' => $breadcumb]);
    }
    /**
     * Delete a given brand
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $brand = $this->brandRepo->findBrandById($id);

        $brandRepo = new BrandRepository($brand);
        $brandRepo->dissociateProducts();
        $brandRepo->deleteBrand();

        return redirect()->route('company.brands.index')->with('message', 'Delete successful!');
    }

}