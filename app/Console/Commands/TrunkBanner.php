<?php

namespace App\Console\Commands;

use App\Auction\Product;
use App\Banner;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TrunkBanner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'banner:trunk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete expired banners';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        Banner::where('end_date', '<', Carbon::now())->each(function ($item) {
        $item->delete();
        
    });
    Product::where('end_date','<',Carbon::now())->each(function($item){
        $item->active = 0;
        $item->save();
    });
    }
}
