<?php

namespace App\Auction;

use App\Shop\Customers\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','National_ID'];

    protected $table = 'auction_users';

    /**
     * To get the full details of a user 
     * @return BelongsTo
     * @author Ziad
     */
    public function parentUser()
    {
        return $this->belongsTo(Customer::class,'user_id');
    }

    
    /**
     * to get user products or auctions
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * to get all user bids
     */
    public function bids()
    {
        return $this->hasMany(Bid::class);
    }
    
}
