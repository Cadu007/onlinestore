<?php

namespace App\Auction;

use Carbon\Carbon;
use App\Shop\Categories\Category;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use SoftDeletes,UploadableTrait;

    protected $table = 'auction_products'; 


    protected $fillable = [
        'name', 'cover','description', 'start_price', 'active', 'end_date', 'slug','cover','min_increment','direct_selling_price','country_id','city_id','address'
    ];

    protected $dates = [
        'end_time', 'deleted_at',
    ];

    /**
     * Define what our unique thing is when we implicitly bind a route param to the model
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return Items Based On Bidders
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTrend($query)
    {
        return $query->where('active',1)->where('approved',1)->orderby('bidders','desc');
    }

    /**
     * The user who created this listing
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Checker for the ability to renew the auction
     * @return bool
     */
    public function checkRenewValidation()
    {
        if($this->renew == 2)
        {
            return false;
        }
        return true;
    }

    /**
     * Renew The Auction 
     * @return bool
     */
    public function renewAuction()
    {
        if($this->checkRenewValidation())
        {
            $date = new Carbon($this->end_date);
            $date = $date->addHours(12);
            $this->renew += 1; 
            $this->end_date = $date;
            return $this->save();
        }
        else
        {
            return false;
        }
    }

    /**
     * Named constructor which takes the title and creates the title and slug parameters
     * of the item. This avoids slug duplication.
     * @param string $title
     * @return static
     */
    public static function createFromTitle(string $title)
    {
        $item        = new static;
        $slug        = str_slug($title);
        $item->name = $title;
        $item->slug  = $slug;
        $i           = 1;
        while (Product::where('slug', $slug)->count() > 0)
        {
            $i++;
            $slug = $item->slug . "-{$i}";
        }
        $item->slug = $slug;

        return $item;
    }

    /**
     * return all products with their current price
     * @return Collection
     */
    public static function getAllProducts() : Collection
    {
        $products = Product::getAllActiveApprovedProducts();
        $products = Product::getAllProductPrices($products);
        return $products;
    }

    /**
     * sort auction product
     * @param string $sort,string $order
     */
    public static function sort(string $sort,bool $order)
    {
        $products = Product::getAllActiveApprovedProducts();
        if($products == null)
        {
            return [];
        }
        else 
        {
            $products = Product::getAllProductPrices($products);
            return $products->sortBy($sort,0,$order);
        }
    }

    /**
     * Check If User Can Delete Or Update The Auction
     * @return bool
     */
    public function checkValidation()
    {
        if($this->bidders != 0)
        {
            return false;
        }
        return true;
    }

    /**
     * Get Remaining Time Of The Auction Product
     * @return string
     */
    public function getRemainingTime()
    {
        $date = new Carbon($this->end_date);
        $now = Carbon::now();
        $diff = $now->diffAsCarbonInterval($date);
        $diff = $diff->hours . ':' . $diff->minutes . ':' . $diff->seconds;
        return $diff;
    }

    public static function getAllActiveApprovedProducts()
    {
        return Product::all()->where('active',1)->where('approved',1);
    }

    /**
     * Get All The Current Price Of All Bids
     * @param mixed $products
     * @return Collection
     */
    public static function getAllProductPrices($products) : Collection
    {
        $products->map(function($product)
        {
            $product->price = $product->getPrice(); 
        });
        return $products;
    }
    /**
     * Get The Price Of The Product Now
     * @return int
     */
    public function getPrice() : int
    {
        if($this->bidders == 0)
        {
            return $this->start_price;
        }
        else
        {
            return $this->currentBid();
        }
    }

    /**
     * Check If the product belong to the logged in user
     * @param Product $product,User $user
     * @return bool
     */
    public function checkUser(User $user) : bool
    {
        if($this->user_id == $user->id)
        {
            return true;
        }
        return false;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('auction/products', ['disk' => 'public']);
    }


        /**
     * @param Collection $collection
     *
     * @return void
     */
    public function saveProductImages(UploadedFile $file)
    {

            $filename = $this->storeFile($file);
            $productImage = new ProductImages([
                'product_id' => $this->id,
                'src' => $filename]);
            $this->images()->save($productImage);
    }


    /**
     * All bids that have been placed on this item.
     * @return HasMany
     */
    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    /**
     * The highest bid by bid amount.
     * @return Bid
     */
    public function currentBid()
    {
        return $this->bids()->orderBy('amount', 'desc')->first() ;
    }

    /**
     * The user name of the user who placed the highest bid.
     * @return string
     */
    public function highBidder()
    {
        return $this->currentBid()->user->name ?? 'No High Bidder';
    }

    /**
     * filter products by min price , max price and categories
     * @param int $min_price
     * @param int $max_price
     * @param array $categories
     * @return Collection
     */
    public static function filter(int $min_price,int $max_price,array $categories)
    {
        $products = Product::getAllActiveApprovedProducts();
        $products = Product::getAllProductPrices($products);
        $products = $products->filter(function($product) use($min_price,$max_price,$categories)
        {
            $bool = false;
            $bool += $product->checkPrice($min_price,$max_price);
            $bool += in_array($product->categories->id,$categories);
            return $bool;
        });
        return $products;
    }

    /**
     * Checker for auction price between the min price and the max price
     * @param int $min_price,
     * @param int $max_price
     * @return bool
     */
    public function checkPrice(int $min_price,int $max_price)
    {
        if($this->getPrice() >= $min_price && $this->getPrice() <= $max_price )
        {
            return true;
        }
        return false;
    }


    /**
     * Checker for auction category
     * @param int $categoryId
     * @return bool
     */
    public function checkCategory(int $categoryId)
    {
        if ( $this->categories()->id == $categoryId ) 
        {
            return true;
        }
        return false;
    }

    /**
     * The user name of the winner of the bid
     * @return Collection
     */
    PUBLIC function winner()
    {
        return $this->bids()->where('winner',1);
    }

    /**
     * Has this item's listing ended meaning is it now later than the item's end time.
     * @return bool
     */
    public function hasEnded()
    {
        return ($this->end_date < Carbon::now());
    }

    /**
     * get All Images Of A Specific Product
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImages::class);
    }

    /**
     * get Category
     * @return BelongsTo
     */

    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}
