<?php

namespace App\Auction;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Bid extends Model
{
    use SoftDeletes;

    protected $table = 'auction_bids'; 

    protected $fillable = ['user_id', 'product_id', 'amount'];

    /**
     * relation between bid and user
     * 
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * relation between bid and user
     * 
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }}
