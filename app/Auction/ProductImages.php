<?php

namespace App\Auction;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProductImages extends Model
{
    use SoftDeletes;
    //
    protected $table = 'auction_productImages';

    protected $fillable = ['product_id','src'];
}
