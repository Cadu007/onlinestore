<?php

namespace App\Mall;

use App\Shop\Countries\Country;
use Illuminate\Database\Eloquent\Model;

class Mall extends Model
{
    //
    protected $table = 'mall_malls';

    protected $fillable = ['name','location','open_time','close_time','map','cover','country_id','city'];

    public function stores()
    {
        return $this->belongsToMany(Store::class, 'mall_malls_stores', 'mall_id', 'store_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
