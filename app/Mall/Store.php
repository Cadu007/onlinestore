<?php

namespace App\Mall;

use App\Shop\Categories\Category;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
    protected $table = 'mall_stores';
    protected $fillable = [
        'name',
        'email',
        'password',
        'cover',
        'mobile',
        'open_time',
        'close_time',
        'category_id',
        'location'

    ];
    protected $hidden = ['email','password'];

    /**
     * @return BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function malls()
    {
        return $this->belongsToMany(Mall::class, 'mall_malls_stores', 'store_id', 'mall_id');
    }

    
}
