<?php

namespace App\Mall;

// use App\Mall\Store;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'mall_products';
    protected $fillable =[
        'name',
        'cover',
        'price',
        'description',
        'store_id'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
