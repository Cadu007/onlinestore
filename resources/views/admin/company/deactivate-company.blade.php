@extends('layouts.admin.app')

@section('content')

    <div class="container" style="margin-top: 50px;">
        <div class="box">
            <div class="box-body">
                <h2>Deactivate Companies</h2>

                <table class="custom-table table" style="margin-top: 20px;">
                    <thead>
                    <tr>
                        <th scope="col">Company ID</th>
                        <th scope="col">Company Name</th>
                        <th scope="col">National Id </th>
                        <th scope="col">Logo</th>
                        <th scope="col">Commercial Licence</th>
                        <th scope="col">Deactivate</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($companies as $company)
                        <tr>
                            <th style="text-align: center">{{$company->id}}</th>
                            <th>{{$company->name}}</th>
                            <th><a target="_blank" href="{{$company->getNationalIdImageUrl()}}"> National ID</a></th>
                            <th><a target="_blank" href="{{$company->getLogoUrl()}}"> Logo</a></th>
                            <th><a target="_blank" href="{{$company->getCommercialLicenceUrl()}}"> Commercial Licence</a></th>
                            <th>
                                <form method="POST" action="{{route("admin.company.unApproveCompany",['id'=>$company->id])}}">
                                    @method('PATCH')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Deactivate</button>
                                </form>
                            </th>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection