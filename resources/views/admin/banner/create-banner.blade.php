@extends('layouts.admin.app')

@section('content')

<div class="container">

<form action="{{route('admin.banner.addBanner')}}" method="POST" enctype="multipart/form-data">
        
        @csrf
        <div class="col-md-6">
            <input type="file" class="custom-file-input" name="banner" id="banner" required>
            <label class="custom-file-label" for="banner">Choose file</label>
            
            @if ($errors->has('banner'))
            <span class="help-block">
                <strong>{{ $errors->first('banner') }}</strong>
            </span>
            @endif
        </div>
        <div>
            <select name="company_id">
                @foreach ($companies as $company)
                <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach
            </select>
        </div>
        
        <input type="date" name="start_date" required>
        <input type="date" name="end_date" required>

        <button type="submit" class="btn btn-primary">Add</button>
    </form>
</div>

@endsection