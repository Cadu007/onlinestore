@extends('layouts.admin.app')

@section('content')

<div class="container">

<form action="{{route('admin.promotion.updatePromotion')}}" method="POST" enctype="multipart/form-data">
        
        @csrf
        @method('PUT')
        <div class="col-md-6">
            <input type="file" class="custom-file-input" name="image_1" id="promotion_1">
            <label class="custom-file-label" for="image_1">Choose Promotion 1</label>
            
            @if ($errors->has('image_1'))
            <span class="help-block">
                <strong>{{ $errors->first('image_1') }}</strong>
            </span>
            @endif
        </div>
       
        <div class="col-md-6">
            <input type="file" class="custom-file-input" name="image_2" id="promotion_2">
            <label class="custom-file-label" for="image_2">Choose Promotion 2</label>
            
            @if ($errors->has('image_2'))
            <span class="help-block">
                <strong>{{ $errors->first('image_2') }}</strong>
            </span>
            @endif
        </div>
       

        <button type="submit" class="btn btn-primary">Add</button>
    </form>
</div>

@endsection