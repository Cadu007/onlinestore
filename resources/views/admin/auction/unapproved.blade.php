@extends('layouts.admin.app')

@section('content')

<div class="container">
    @foreach ($products as $product)
    <form action="{{url("admin/auction/approve/{$product->id}")}}" method="GET">
        @csrf
        <b>Product Name</b> {{$product->name}} </br>
        <b>Product Owner</b> {{$product->user->parentUser->name}} </br>
        <b>Product Cover</b> {{$product->cover}} </br>
        <b>Product Direct Selling Price</b> {{$product->direct_selling_price}} </br>
        <b>Product Start Price</b> {{$product->start_price}} </br>
        <b>Product Minmum Increament</b> {{$product->min_increment}} </br>
        <button type="submit">Approve</button>
    </form>
@endforeach




</div>

@endsection