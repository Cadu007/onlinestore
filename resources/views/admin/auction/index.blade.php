@extends('layouts.admin.app')

@section('content')

<div class="container">


@foreach ($bids as $bid)
    <b>Winner:</b>{{$bid->user->parentUser->name}} <!-- bid winner -->
    <br>
    <b>Product:</b>{{$bid->product->name}} <!-- product name -->
    <br>
   <b>Product Winner</b> {{$bid->product->user->parentUser->name}} <!-- product owner -->
   <br>
   <b>Sold Price</b> {{$bid->amount}}
@endforeach
</div>

@endsection