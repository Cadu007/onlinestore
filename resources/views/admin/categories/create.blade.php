@extends('layouts.admin.app')

@section('content')
<!-- Main content -->
<section class="content" style="margin-top: 25px;" >
    @include('layouts.errors-and-messages')
    <div style="margin:0 auto; width:50%; border:none; text-align:center;">
        <h2>Add Category</h2>
    </div>
    <div class="box customBox">
        <form action="{{ route('admin.categories.store') }}" method="post" class="form" enctype="multipart/form-data">
            <div class="box-body">
                {{ csrf_field() }}
               <div class="form-group group mt-3">
                        <label for="parent">Parent Category</label>
                        <select name="parent" id="parent" class="form-control select2">
                            <option value="0">No Parent</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group group">
                    <label for="name">Category / Sub Category Name <span class="text-danger">*</span></label>
                    <input type="text" name="name" id="name" placeholder="Name" class="form-control"
                        value="{{ old('name') }}">
                    </div>
               

                <div class="form-group group">
                    <label for="iconCode">Category Icon Code <span class="text-danger">*</span></label>
                    <input type="text" name="icon_code" id="icon_code" placeholder="Icon Code" class="form-control">
                </div>

                <div class="form-group group">
                    <label for="icon_font">Category Icon Font <span class="text-danger">*</span></label>
                    <input type="text" name="icon_font" id="icon_font" placeholder="Icon Font" class="form-control">
                </div>

                <div class="hint">
                    <p style="padding: 5px;">
                        You can get Icon Code & Icon Font Family From <a class="flutterIcon" href="http://fluttericon.com/">Here</a>
                    </p>
                </div>

                <div class="form-group group">
                    <label for="f_color">Category First Color <span class="text-danger">*</span></label>
                    <input class="form-control" name="f_color" id="f_color" placeholder="First Color" type="text" />
                </div>

                <div class="form-group group">
                    <label for="l_color">Category Second Color <span class="text-danger">*</span></label>

                    <input class="form-control" name="l_color" id="l_color" placeholder="Second Color" type="text" />
                </div>

                <div class="group">
                    <label for="Attr">Category Attribues <span class="text-danger form-group">*</span></label>
                    <div id="Attr">
                        @foreach($attributes as $attr)
                        <input type="checkbox" name="Attr[]" value="{{ $attr->id }}"> &nbsp;{{ $attr->name }} <br />
                        @endforeach
                    </div>
                </div>
                <div class="form-group mt-3 group">
                    <label for="cover">Sub Category Cover </label>
                    <input type="file" name="cover" id="cover" class="form-control">
                </div>
                 <div class="form-group group">
                        <label for="status">Status </label>
                        <select name="status" id="status" class="form-control">
                            <option value="0">Disable</option>
                            <option value="1">Enable</option>
                        </select>
                    </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer" style="text-align:center;">
                <div class="btn-group" style="margin:0 auto;">
                    <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection