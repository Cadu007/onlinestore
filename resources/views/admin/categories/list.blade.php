@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content" style="margin-top: 25px;" >
    <div style="margin:0 auto; width:50%; border:none; text-align:center;">
<h2>Show Categories</h2>
</div>
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($categories)
            <div class="box">
                <div class="box-body">
                    <h2>Categories</h2>
                    <table class="table">
                       
                        
                        <thead>
                            <tr>
                                <td class="col-md-1">Name</td>
                                <td class="col-md-1">Slug</td>
                                <td class="col-md-1">Icon Code</td>
                                <td class="col-md-1">Icon Font Family</td>
                                <td class="col-md-1">First Color</td>
                                <td class="col-md-1">Second Color</td>
                                <td class="col-md-1">Attributes</td>
                                <td class="col-md-1">Status</td>
                                <td class="col-md-1">Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td>
                                    <a href="{{ route('admin.categories.show', $category->id) }}">{{ $category->name }}</a>
                                </td>
                                <td>{{ $category->slug }}</td>
                                <td>{{ $category->icon_code }}</td>
                                <td>{{ $category->icon_font }}</td>
                                <td>{{ $category->f_color }}</td>
                                <td>{{ $category->l_color }}</td>
                                <td> {{ $category->attributes }}
                                </td>
                                <td>@include('layouts.status', ['status' => $category->status])</td>
                                <td>
                                    <form action="{{ route('admin.categories.destroy', $category->id) }}" method="post" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <div class="btn-group">
                                            @if($category->name != "UnCategorized")
                                            <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                            @endif
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
{{--                    {{ $categories->links() }}--}}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection
