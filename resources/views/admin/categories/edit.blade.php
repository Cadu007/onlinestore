@extends('layouts.admin.app')

@section('content')
<!-- Main content -->
<section class="content" style="margin-top: 25px;" >
    @include('layouts.errors-and-messages')
    <div style="margin:0 auto; width:50%; border:none; text-align:center;">
        <h2>Edit Category</h2>
    </div>
    <div class="box customBox">
        <form action="{{ route('admin.categories.update', $category->id) }}" method="post" class="form"
            enctype="multipart/form-data">
            <div class="box-body">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <div class="form-group group mt-3">
                        <label for="parent">Parent Category</label>
                        <select name="parent" id="parent" class="form-control select2">
                            <option value="0">No Parent</option>
                            @foreach($categories as $cat)
                                <option @if($cat->id == $category->parent_id) selected="selected" @endif value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group group">
                    <label for="name">Category Name <span class="text-danger">*</span></label>
                    <input type="text" name="name" id="name" placeholder="Name" class="form-control"
                        value="{!! $category->name ?: old('name')  !!}">
                </div>
                <div class="form-group group">
                    <label for="name">Category Slug <span class="text-danger">*</span></label>
                    <input type="text" name="slug" id="slug" placeholder="slug" class="form-control"
                        value="{!! $category->slug ?: old('slug')  !!}">
                </div>
                <div class="form-group group">
                    <label for="icon_code">Category Icon Code <span class="text-danger">*</span></label>
                    <input type="text" name="icon_code" id="icon_code" placeholder="Icon Code" class="form-control"
                        value="{!! $category->icon_code ?: old('icon_code')  !!}">
                </div>
                <div class="form-group group">
                    <label for="icon_font">Category Icon Font Family <span class="text-danger">*</span></label>
                    <input type="text" name="icon_font" id="icon_font" placeholder="Icon Font Family"
                        class="form-control" value="{!! $category->icon_font ?: old('icon_font')  !!}">
                </div>
                <div style="background-color:gold; text-align:center; margin:0 auto; width:50%;">
                    <p>
                        You can get Icon Code & Icon Font Family From <a href="http://fluttericon.com/">Here</a>
                    </p>
                </div>
                <div class="form-group group">
                    <label for="f_color">Category First Color <span class="text-danger">*</span></label>
                    <input type="text" name="f_color" id="f_color" placeholder="First Color" class="form-control"
                        value="{!! $category->f_color ?: old('f_color')  !!}">
                </div>
                <div class="form-group group">
                    <label for="l_color">Category Second Color <span class="text-danger">*</span></label>
                    <input type="text" name="l_color" id="l_color" placeholder="Second Color" class="form-control"
                        value="{!! $category->l_color ?: old('l_color')  !!}">
                </div>
                <div class="group">
                        <label for="Attr">Category Attributes <span class="text-danger form-group">*</span></label>
                    <div>
                        @foreach($attributes as $attr)
                            <input type="checkbox" name="Attr[]" value="{{ $attr->id }}" @if( in_array($attr->id, $categoryAttributes)) checked="checked" @endif > &nbsp;{{ $attr->name }}<br />
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="group">
             @if(isset($category->cover))
                <div class="form-group mt-3">
                    <img class="mt-3" src="{{ asset("storage/$category->cover") }}" alt="" class="img-responsive">
                    <br />
                    <a onclick="return confirm('Are you sure?')"
                    href="{{ route('admin.category.remove.image', ['category' => $category->id]) }}"
                    class="btn btn-danger">Remove image?</a>
                </div>
                @endif
            </div>
                <div class="form-group mt-3 group"><div class=""><div class=""><div class="">

                </div></div></div>
                    <label for="cover">Cover </label>
                    <input type="file" name="cover" id="cover" class="form-control">
                </div>
                <div class="group">
                    <label for="status">Status </label>
                    <select name="status" id="status" class="form-control">
                        <option value="0" @if($category->status == 0) selected="selected" @endif>Disable</option>
                        <option value="1" @if($category->status == 1) selected="selected" @endif>Enable</option>
                    </select>
                </div>
</div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="text-align:center;">
        <div class="btn-group" style="margin:0 auto;">
            <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Back</a>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>
    </form>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection
