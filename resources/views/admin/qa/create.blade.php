@extends('layouts.admin.app')

@section('content')

<div class="container">

<form action="{{url('questionAnswer/store')}}" method="POST" enctype="multipart/form-data">
        
        @csrf
        <input type="text" name="question">
        <textarea name="answer"></textarea>
        <button type="submit" class="btn btn-primary">Add</button>
    </form>
</div>

@endsection