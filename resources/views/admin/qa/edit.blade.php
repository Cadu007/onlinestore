@extends('layouts.admin.app')

@section('content')

<div class="container">

<form action="{{url("questionAnswer/update/{$qa->id}")}}" method="POST" enctype="multipart/form-data">
        
        @csrf
        @method('PATCH')
<input type="text" name="question" value="{{$qa->question}}">
<textarea name="answer">{{$qa->answer}}</textarea>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

@endsection