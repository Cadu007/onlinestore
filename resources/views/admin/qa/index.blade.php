@extends('layouts.admin.app')

@section('content')

<div class="container">

    @foreach ($qa as $qa)
    
    <form action="{{url("questionAnswer/delete/{$qa->id}")}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('delete')
       
<input type="text" name="question" value="{{$qa->question}}">
<textarea name="answer">{{$qa->answer}}</textarea>
        <a href="questionAnswer/edit/{{$qa->id}}" class="btn btn-success">Edit </a>
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
    @endforeach
</div>
<script>

    </script>
@endsection