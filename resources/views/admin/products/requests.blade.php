@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content" style="margin-top: 25px;" >
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$products->isEmpty())
            <div class="box">
                <div class="box-body">
                    <h2>Products Requests</h2>
                    @include('admin.shared.requests')
                    {{ $products->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection
