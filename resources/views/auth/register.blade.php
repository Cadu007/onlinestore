@extends('layouts.front.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #563d7c; color: white;">Company Registration</div>
                <div class="panel-body" style="background: whitesmoke;">
                    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/company') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus required>

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" required>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('country_code') ? ' has-error' : '' }}">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select class="form-control" name="country_code" required>
                                    <option value="0"></option>
                                    @foreach( $countries as $country )
                                        <option value="{{ $country->iso }}">{{$country->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('country_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                            <label for="commercial_licence" class="col-md-4 control-label">Logo</label>
                            <div class="col-md-6">
                                <input type="file" class="custom-file-input" name="logo" id="logo" required>
                                <label class="custom-file-label" for="logo">Choose file</label>

                                @if ($errors->has('logo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('commercial_licence') ? ' has-error' : '' }}">
                            <label for="commercial_licence" class="col-md-4 control-label">Company's Commercial Licence</label>
                            <div class="col-md-6">
                                <input type="file" class="custom-file-input" name="commercial_licence" id="commercial_licence" required>
                                <label class="custom-file-label" for="commercial_licence">Choose file</label>

                                @if ($errors->has('commercial_licence'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('commercial_licence') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('national_id') ? ' has-error' : '' }}">
                            <label for="national_id" class="col-md-4 control-label">National ID</label>
                            <div class="col-md-6">
                                <input type="file" class="custom-file-input" name="national_id" id="national_id" required>
                                <label class="custom-file-label" for="national_id">Choose file</label>

                                @if ($errors->has('national_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('national_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('termsAndConditions') ? ' has-error' : '' }}">
                            <label for="termsAndConditions" class="col-md-4 control-label">Accept Conditions</label>
                            <div class="col-md-1">
                                <input id="termsAndConditions" type="checkbox" class="form-check-input" name="termsAndConditions" required>

                                @if ($errors->has('termsAndConditions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('termsAndConditions') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" style="background: #563d7c">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
