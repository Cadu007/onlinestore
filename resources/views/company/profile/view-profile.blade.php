@extends('company.layouts.company.app')

@section('content')

<div class="box">
    <div class="box-body">
        <div class="card">
            <h5 class="card-header">{{$company->name}}</h5>
            <div class="card-body">
                <h5 class="card-title">Phone: {{$company->phone}}</h5>
                <h5 class="card-title">Email: {{$company->email}}</h5>
                <h5 class="card-title">Commercial Licence: <a target="_blank" href="{{$company->getCommercialLicenceUrl()}}" class="btn btn-warning">Show</a></h5>
                <h5 class="card-title">National ID: <a target="_blank" href="{{$company->getNationalIdImageUrl()}}" class="btn btn-warning">Show</a></h5>
                <h5 class="card-title">Country: {{$company->country}}</h5>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div class="btn-group">
            <a href="{{ url('company') }}" class="btn btn-default btn-sm">Back</a>
        </div>
    </div>
</div>
<!-- /.box -->



@endsection