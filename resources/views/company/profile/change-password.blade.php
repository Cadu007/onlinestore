@extends('company.layouts.company.app')

@section('content')


<div class="container">

    
    <form method="POST" action="/company/update-password/{{auth('company')->user()->id}}">
        @method('PATCH')
        @csrf

  
    <div class="form-group">
        <label for="exampleInputPassword2">New Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword2" placeholder="New Password">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">New Password Confirmation</label>
        <input type="password" name="confirm_password" class="form-control" id="exampleInputPassword1" placeholder="New Password Confirmation">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

@endsection