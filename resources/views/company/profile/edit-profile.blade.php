@extends('company.layouts.company.app')

@section('content')


<div class="container">

    
    <form method="POST" action="/company/{{auth('company')->user()->id}}">
        @method('PATCH')
        @csrf

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{auth('company')->user()->email}}">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
        <div class="form-group">
            <label for="exampleName">Name</label>
        <input type="text" name="name" class="form-control" id="exampleName" value="{{auth('company')->user()->name}}">
     
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

@endsection