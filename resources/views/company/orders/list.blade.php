@extends('company.layouts.company.app')

@section('content')
    <!-- Main content -->
    <section class="content" style="margin-top: 25px;" >

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($orders)
            <div class="box">
                <div class="box-body">
                    <h2>Orders</h2>
                    @include('layouts.search', ['route' => route('company.orders.index')])
                    <table class="table">
                        <thead>
                            <tr>
                                <td class="col-md-4">ID</td>
                                <td class="col-md-4">Order ID</td>
                                <td class="col-md-4">Product ID</td>
                                <td class="col-md-4">Product Price</td>
                                <td class="col-md-4">Quantity</td>
                                <td class="col-md-4">Product Name</td>
                                <td class="col-md-4">Product Sku</td>
                                <td class="col-md-4">Product Description</td>
                                <td class="col-md-4">Product Combination</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->order_id}}</td>
                                <td>{{ $order->product_id }}</td>
                                <td>{{ config('cart.currency') }} {{ $order->product_price }}</td>
                                <td> {{$order->quantity}} </td>
                                <td> {{$order->product_name}} </td>
                                <td> {{$order->product_sku}} </td>
                                <td> {{$order->product_description}} </td>
                                <td> {{$order->product_combination}} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection