

@extends('company.layouts.company.app')

@section('content')


<div class="box">
    <div class="box-body">
        <table class="table">
            <thead>
            <tr>
                <td class="col-md-2">Address Alias</td>
                <td class="col-md-3">Address !</td>
                <td class="col-md-3">Address 2</td>
                <td class="col-md-2">Zip Code</td>
                <td class="col-md-2">City</td>
                <td class="col-md-2">Country Codey</td>
            </tr>
            </thead>
            <tbody>
                @foreach ($company->addresses as $address)
                <tr>
                       <td> {{$address->alias}}</td>
                        <td>{{$address->address_1}}</td>
                        <td>{{$address->address_2}}</td>
                       <td> {{$address->zip}}</td>
                       <td> {{$address->city}}</td>
                       <td> {{$address->state_code}}</td>
                       
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div class="btn-group">
            <a href="{{ url('company') }}" class="btn btn-default btn-sm">Back</a>
        </div>
    </div>
</div>
<!-- /.box -->



@endsection
