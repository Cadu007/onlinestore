
<div class="form-group">
<ul class="checkbox-list">
    @foreach($categories as $category)
        <li>
                    @if($category->parent_id != null)
                <label style="font-weight:light; color:darkgrey;">
                    <input
                            type="radio"
                            @if(isset($selectedIds) &&  $selectedIds == $category->id)checked="checked" @endif
                            name="categories[]"
                            value="{{ $category->id }}">
                    {{ $category->name }}
                    
                </label>
                     @else
                        <label style="font-weight:bold; color:darkgray; text-decoration:underline;">{{$category->name}}:</label>
                    @endif
        </li>
        @if($category->children->count() >= 1)
            @include('admin.shared.categories', ['categories' => $category->children, 'selectedIds' => $selectedIds])
        @endif
    @endforeach
</ul>
</div>