@extends('company.layouts.company.app')

@section('content')
    <!-- Main content -->
    <section class="content" style="margin-top: 25px;" >
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$products->isEmpty())
            <div class="box">
                <div class="box-body">
                    <h2>Products</h2>
                    @include('layouts.search', ['route' => route('company.products.index')])
                    @include('company.shared.products')
                    {{ $products->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection
