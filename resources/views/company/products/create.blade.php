@extends('company.layouts.company.app')

@section('content')
<!-- Main content -->
<section class="content" style="margin-top: 25px;" >
    @include('layouts.errors-and-messages')
    <div style="margin:0 auto; width:50%; border:none; text-align:center;">
        <h2>Add Product</h2>
    </div>

    @if( ! $brands->isEmpty() )
        <div class="box customBox">
            <form action="{{ route('company.products.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}

                    <div class="form-group group">
                        <label for="parent">Category <span class="text-danger">*</span></label>
                        <select name="parent" id="parent" class="form-control">
                            <option value="0">Select</option>
                            @foreach($categories as $category)
                                @if($category->children->count() >= 1 )
                                    <optgroup label="{{ $category->name }}">
                                        @foreach($category->children as $child)
                                            <option value="{{$child->id}}">
                                                {{$child->name}}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group group">
                        <label for="name">Product Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control"
                               value="{{ old('name') }}">
                    </div>

                    <div class="form-group group">
                        <label for="description">Description </label>
                        <textarea class="form-control" name="description" id="description" rows="5"
                                  placeholder="Description">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group group">
                        <label for="cover">Cover <span class="text-danger">*</span></label>
                        <input type="file" name="cover" id="cover" style="padding:0px; border: none; outline: none;" class="form-control">
                    </div>

                    <div class="form-group group">
                        <label for="image">Images <span class="text-danger">*</span></label>
                        <input type="file" name="image[]" id="image" style="padding:0px; border: none; outline: none;" class="form-control" multiple>
                        <small class="text-warning">You can use ctr (cmd) to select multiple images</small>
                    </div>

                    <div class="form-group group">
                        <label for="quantity">Quantity <span class="text-danger">*</span></label>
                        <input type="text" name="quantity" id="quantity" placeholder="Quantity" class="form-control"
                               value="{{ old('quantity') }}">
                    </div>

                    <div class="form-group group">
                        <label for="price">Price <span class="text-danger">*</span></label>
                        <input type="text" name="price" id="price" placeholder="Price" class="form-control"
                               value="{{ old('price') }}">
                    </div>

                    <div class="form-group group">
                        <label for="sale_price">Price After Discount<span class="text-danger">*</span></label>
                        <input type="text" name="sale_price" id="sale_price" placeholder="Price" class="form-control" value="{{ old('price') }}">
                    </div>

                    <div class="form-group group">
                        <label for="sku">SKU <span class="text-danger">*</span></label>
                        <input type="text" name="sku" id="sku" placeholder="xxxxx" class="form-control" value="{{ old('sku') }}">
                    </div>

                    <div class="form-group group">
                        <label for="length">Length <span class="text-danger">*</span></label>
                        <input type="text" name="length" id="length" placeholder="length" class="form-control" value="">
                    </div>

                    <div class="form-group group">
                        <label for="width">Width <span class="text-danger">*</span></label>
                        <input type="text" name="width" id="width" placeholder="width" class="form-control" value="">
                    </div>

                    <div class="form-group group">
                        <label for="height">Height <span class="text-danger">*</span></label>
                        <input type="text" name="height" id="height" placeholder="height" class="form-control" value="">
                    </div>

                    <div class="form-group group">
                        <label for="distance_unit">Distance Unit <span class="text-danger">*</span></label>
                        <input type="text" name="distance_unit" id="distance_unit" placeholder="distance unit" class="form-control" value="">
                    </div>

                    @if( ! $brands->isEmpty() )
                        <div class="form-group group">
                            <label for="brand_id">Brand <span class="text-danger">*</span></label>
                            <select name="brand" id="brand" class="form-control select2">
                                <option value=""></option>
                                @foreach($brands as $brand)
                                    <option @if(old('brand_id')==$brand->id) selected="selected" @endif
                                    value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="form-group group">
                        @include('company.shared.status-select', ['status' => 0])
                    </div>

                    <div class="form-group group">
                        @include('company.shared.attribute-select', [compact('default_weight')])
                    </div>

                    <div class="box-footer" style="text-align:center;">
                        <div class="btn-group">
                            <a href="{{ route('company.products.index') }}" class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </div>
            </form>
        </div>
    @else
        <div class="alert alert-secondary alert-warning" role="alert" style="font-size: 15px;">
            You need to add Brands before adding product <a href="{{ route('company.brands.create') }}">Create Brand</a>
        </div>
    @endif


    <!-- /.box -->

</section>
<!-- /.content -->
@endsection