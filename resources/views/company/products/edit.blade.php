@extends('company.layouts.company.app')

@section('content')
<!-- Main content -->
<section class="content" style="margin-top: 25px;" >
    @include('layouts.errors-and-messages')
    <div style="margin:0 auto; width:50%; border:none; text-align:center;">
        <h2>Edit Product</h2>
    </div>
    <div class="box customBox">
        <form action="{{ route('company.products.update', $product->id) }}" method="post" class="form"
            enctype="multipart/form-data">
            <div class="box-body">
                <div class="row">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
                    <div class="col-md-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist" id="tablist">
                                <li role="presentation" @if(!request()->has('combination')) class="active" @endif><a href="#info" aria-controls="home" role="tab" data-toggle="tab">Info</a></li>
                                <li role="presentation" @if(request()->has('combination')) class="active" @endif><a href="#combinations" aria-controls="profile" role="tab" data-toggle="tab">Combinations</a></li>
                            </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" id="tabcontent">
                            <div role="tabpanel" class="tab-pane @if(!request()->has('combination')) active @endif"
                                id="info">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <h2 style="color:darkblue;">{!! $product->name !!}</h2> -->
                                        <div class="form-group group">
                                            <label for="parent">Parent Category</label>
                                            <select name="parent" id="parent" class="form-control">
                                                <option value="0">Select</option>
                                                @foreach($categories as $category)
                                                @if($category->children->count() >= 1 )
                                                <optgroup label="{{ $category->name }}">
                                                    @foreach($category->children as $child)
                                                    <option value="{{$child->id}}">
                                                        {{$child->name}}
                                                    </option>
                                                    @endforeach
                                                </optgroup>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group group">
                                            <label for="name">Product Name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Name"
                                                class="form-control" value="{!! $product->name !!}">
                                        </div>
                                        <div class="form-group group">
                                            <label for="description">Product Description </label>
                                            <textarea class="form-control ckeditor" name="description" id="description"
                                                rows="5"
                                                placeholder="Description">{!! $product->description  !!}</textarea>
                                        </div>
                                        <div class="form-group group mb-1">
                                                    <img src="{{ $product->cover }}" alt=""
                                                        class="img-responsive img-thumbnail">
                                        </div>
                                        <div class="form-group group">
                                            <label for="cover">Cover </label>
                                            <input type="file" name="cover" id="cover" style="padding:0px; border: none; outline: none;" class="form-control">
                                        </div>
                                        <div class="form-group group">
                                            @foreach($images as $image)
                                            <img src="{{  asset("storage/$image->src") }}" alt=""
                                                class="img-responsive img-thumbnail"> <br /> <br>
                                            <a onclick="return confirm('Are you sure?')"
                                                href="{{ route('company.product.remove.thumb', ['src' => $image->src]) }}"
                                                class="btn btn-danger btn-sm btn-block">Remove?</a><br />

                                            @endforeach
                                        </div>
                                        <div class="form-group group">
                                            <label for="image">Images </label>
                                            <input type="file" name="image[]" id="image" style="padding:0px; border: none; outline: none;" class="form-control" multiple>
                                            <span class="text-warning">You can use ctr (cmd) to select multiple
                                                images</span>
                                        </div>
                                        <div class="form-group group">
                                            <label for="quantity">Quantity <span class="text-danger">*</span></label>
                                            @if($productAttributes->isEmpty())
                                            <input type="text" name="quantity" id="quantity" placeholder="Quantity"
                                                class="form-control" value="{!! $product->quantity  !!}">
                                            @else
                                            <input type="hidden" name="quantity" value="{{ $qty }}">
                                            <input type="text" value="{{ $qty }}" class="form-control" disabled>
                                            @endif
                                            @if(!$productAttributes->isEmpty())<span class="text-danger">Note: Quantity
                                                is disabled. Total quantity is calculated by the sum of all the
                                                combinations.</span> @endif
                                        </div>
                                        <div class="form-group group">
                                            <label for="price">Currency <span class="text-danger">*</span></label>
                                                <select name="currency" id="currency" class="form-control select2">
                                                    <option value="0">Select</option>
                                                </select>
                                        </div>
                                        <div class="form-group group">
                                            <label for="price">Price</label>
                                            @if($productAttributes->isEmpty())
                                                <!-- <span class="input-group-addon">{{ config('cart.currency') }}</span> -->
                                                <input type="text" name="price" id="price" placeholder="Price"
                                                    class="form-control" value="{!! $product->price !!}">
                                            @else
                                            <input type="hidden" name="price" value="{!! $product->price !!}">
                                            <div class="input-group">
                                                <!-- <span class="input-group-addon">{{ config('cart.currency') }}</span> -->
                                                <input type="text" id="price" placeholder="Price" class="form-control"
                                                    value="{!! $product->price !!}" disabled>
                                            </div>
                                            @endif
                                            @if(!$productAttributes->isEmpty())<span class="text-danger">Note: Price is
                                                disabled. Price is derived based on the combination.</span> @endif
                                        </div>
                                        <div class="form-group group">
                                            <label for="sale_price">Price After Discount</label>
                                                <!-- <span class="input-group-addon">{{ config('cart.currency') }}</span> -->
                                                <input type="text" name="sale_price" id="sale_price"
                                                    placeholder="Sale Price" class="form-control"
                                                    value="{!! $product->sale_price !!}">
                                        </div>
                                        <div class="form-group group">
                                            <label for="sku">SKU <span class="text-danger">*</span></label>
                                            <input type="text" name="sku" id="sku" placeholder="xxxxx"
                                                class="form-control" value="{!! $product->sku !!}">
                                        </div>


                                        <div class="form-group group">
                                            <label for="length">Length <span class="text-danger">*</span></label>
                                            <input type="text" name="length" id="length" placeholder="length"
                                                class="form-control" value="{!! $product->length !!}">
                                        </div>

                                        <div class="form-group group">
                                            <label for="width">Width <span class="text-danger">*</span></label>
                                            <input type="text" name="width" id="width" placeholder="width"
                                                class="form-control" value="{!! $product->width !!}">
                                        </div>

                                        <div class="form-group group">
                                            <label for="height">Height <span class="text-danger">*</span></label>
                                            <input type="text" name="height" id="height" placeholder="height"
                                                class="form-control" value="{!! $product->height !!}">
                                        </div>

                                        <div class="form-group group">
                                            <label for="distance_unit">Distance Unit <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" name="distance_unit" id="distance_unit"
                                                placeholder="distance unit" class="form-control"
                                                value="{!! $product->distance_unit !!}">
                                        </div>
                                        @if(!$brands->isEmpty())
                                        <div class="form-group group">
                                            <label for="brand_id">Brand </label>
                                            <select name="brand" id="brand_id" class="form-control select2">
                                                <option value=""></option>
                                                @foreach($brands as $brand)
                                                <option @if($brand->id == $product->brand_id) selected="selected" @endif
                                                    value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endif


                                        <div class="form-group group">
                                            @include('company.shared.status-select', ['status' => $product->status])
                                        </div>

                                        <div class="form-group group">
                                            @include('company.shared.attribute-select', [compact('default_weight')])
                                        </div>

                                        <!-- /.box-body -->
                                    </div>
                                    <!-- <div class="col-md-4">
                                            <h2>Categories</h2>
                                            @include('admin.shared.categories', ['categories' => $categories, 'ids' => $product])
                                        </div> -->
                                </div>
                                <div class="row">
                                    <div class="box-footer" style="text-align:center; margin:0 auto;">
                                        <div class="btn-group">
                                            <a href="{{ route('company.products.index') }}"
                                                class="btn btn-default btn-sm">Back</a>
                                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane @if(request()->has('combination')) active @endif"
                                id="combinations">
                                <div class="row">
                                    <div class="col-md-4">
                                        @include('company.products.create-attributes', compact('attributes'))
                                    </div>
                                    <div class="col-md-8">
                                        @include('company.products.attributes', compact('productAttributes'))
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
@endsection
@section('css')
<style type="text/css">
    label.checkbox-inline {
        padding: 10px 5px;
        display: block;
        margin-bottom: 5px;
    }

    label.checkbox-inline>input[type="checkbox"] {
        margin-left: 10px;
    }

    ul.attribute-lists>li>label:hover {
        background: #3c8dbc;
        color: #fff;
    }

    ul.attribute-lists>li {
        background: #eee;
    }

    ul.attribute-lists>li:hover {
        background: #ccc;
    }

    ul.attribute-lists>li {
        margin-bottom: 15px;
        padding: 15px;
    }
</style>
@endsection
@section('js')
<script type="text/javascript">
    function backToInfoTab() {
        $('#tablist > li:first-child').addClass('active');
        $('#tablist > li:last-child').removeClass('active');

        $('#tabcontent > div:first-child').addClass('active');
        $('#tabcontent > div:last-child').removeClass('active');
    }
    $(document).ready(function () {
        const checkbox = $('input.attribute');
        $(checkbox).on('change', function () {
            const attributeId = $(this).val();
            if ($(this).is(':checked')) {
                $('#attributeValue' + attributeId).attr('disabled', false);
            } else {
                $('#attributeValue' + attributeId).attr('disabled', true);
            }
            const count = checkbox.filter(':checked').length;
            if (count > 0) {
                $('#productAttributeQuantity').attr('disabled', false);
                $('#productAttributePrice').attr('disabled', false);
                $('#salePrice').attr('disabled', false);
                $('#default').attr('disabled', false);
                $('#createCombinationBtn').attr('disabled', false);
                $('#combination').attr('disabled', false);
            } else {
                $('#productAttributeQuantity').attr('disabled', true);
                $('#productAttributePrice').attr('disabled', true);
                $('#salePrice').attr('disabled', true);
                $('#default').attr('disabled', true);
                $('#createCombinationBtn').attr('disabled', true);
                $('#combination').attr('disabled', true);
            }
        });
    });
</script>
@endsection