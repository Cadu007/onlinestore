<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ $company->getLogoUrl() }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">HOME</li>
            <li><a href="{{ route('company.dashboard') }}"> <i class="fa fa-home"></i> Home</a></li>
            <li class="header">PROFILE </li>
                <li class="@if(request()->segment(2) == 'addresses') active @endif">
                <a href="#"><i class="fa fa-id-badge"></i> Profile
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                <li><a href="{{ url("company/view-profile/{$user->id}")}}"><i class="fa fa-user"></i> View Profile</a></li>
                <li><a href="{{ url("company/edit-profile/{$user->id}/edit")}}"><i class="fa fa-edit"></i> Edit Profile</a></li>
                <li><a href="{{ url("company/change-password/{$user->id}")}}"><i class="fa fa-edit"></i> Change Password</a></li>
                </ul>
                <li class="@if(request()->segment(2) == 'addresses') active @endif">
                <a href="#"><i class="fa fa-map-marker"></i> Addresses
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                <li><a href="{{ url("company/addresses/{$user->id}")}}"><i class="fa fa-circle-o"></i> List addresses</a></li>
                <li><a href="{{ url('company/address/create') }}"><i class="fa fa-plus"></i> Create address</a></li>
                </ul>
            </li>
            <li class="header">SELL</li>
            <li class="treeview @if(request()->segment(2) == 'products' || request()->segment(2) == 'attributes' || request()->segment(2) == 'brands') active @endif">
                <a href="#">
                    <i class="fa fa-gift"></i> <span>Products</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('company.products.index') }}"><i class="fa fa-circle-o"></i> List products</a></li>
                    <li><a href="{{ route('company.products.create') }}"><i class="fa fa-plus"></i> Create product</a></li>
                    <li class="@if(request()->segment(2) == 'brands') active @endif">
                    <a href="#">
                        <i class="fa fa-tag"></i> <span>Brands</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('company.brands.index') }}"><i class="fa fa-circle-o"></i> List brands</a></li>
                        <li><a href="{{ route('company.brands.create') }}"><i class="fa fa-plus"></i> Create brand</a></li>
                    </ul>
                    </li>
                </ul>
            </li>
            <li class="header">ORDERS</li>
            <li class="treeview @if(request()->segment(2) == 'orders') active @endif">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Orders</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('company.orders.index') }}"><i class="fa fa-circle-o"></i> List orders</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->