<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Shop\Companies\Company;

$factory->define(Company::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->firstName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'country_code' => 'EG',
        'phone' => '01281694875',
        'authorized' => true,
        'national_id' => $faker->image('public/storage/companies',400,300, null, false),
        'commercial_licence' => $faker->image('public/storage/companies',400,300, null, false),
        'logo' => $faker->image('public/storage/companies',400,300, null, false),
        'status' => 1
    ];
});
