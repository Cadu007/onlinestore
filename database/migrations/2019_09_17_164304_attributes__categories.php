<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttributesCategories extends Migration
{
    CONST TABLE_NAME = 'Attributes_Categories';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(self::TABLE_NAME,function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('attribute_id');
            $table->unsignedInteger('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
