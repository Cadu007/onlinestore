<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysAttributeCategories extends Migration
{
    CONST TABLE_NAME = 'Attributes_Categories';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table(self::TABLE_NAME,function (Blueprint $table)
        {
            $table->dropForeign(['category_id']);
            $table->dropForeign(['attribute_id']);
        });
    }
}
