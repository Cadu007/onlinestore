<?php

use Kalnoy\Nestedset\NestedSet;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('status')->default(0);
            $table->string('f_color')->nullable();
            $table->string('l_color')->nullable();
            $table->string('icon_code')->nullable();
            $table->string('icon_font')->nullable();
            $table->timestamps();
            NestedSet::columns($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {

            $sm = Schema::getConnection()->getDoctrineSchemaManager();

            $doctrineTable = $sm->listTableDetails('categories');

            if ($doctrineTable->hasIndex('categories__lft__rgt_parent_id_index')) 
            {
                $table->dropIndex('categories__lft__rgt_parent_id_index');
            }

        });

        Schema::dropIfExists('categories');
    }
}
