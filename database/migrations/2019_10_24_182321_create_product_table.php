<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('name');
            $table->string('slug');
            $table->string('cover')->nullable();
            $table->text('description')->nullable();
            $table->integer('start_price');
            $table->integer('min_increment');
            $table->boolean('active')->default(1);
            $table->dateTime('end_date');
            $table->unsignedInteger('bidders')->default(0);
            $table->string('address')->nullable();
            $table->tinyInteger('renew')->default(0);
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            // $table->unsignedInteger('country_id');
            $table->string('country');
            $table->string('city');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('auction_users')
                ->onDelete('cascade');

            // $table->foreign('country_id')
            //     ->references('id')->on('countries')
            //     ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_products');
    }
}
