<?php

use App\Shop\Companies\Company;
use App\Shop\Permissions\Permission;
use App\Shop\Roles\Repositories\RoleRepository;
use App\Shop\Roles\Role;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    public function run()
    {
        $createProductPerm = Permission::find(1);

        $viewProductPerm = Permission::find(2);

        $updateProductPerm = Permission::find(3);

        $deleteProductPerm = Permission::find(4);

        $company = factory(Company::class)->create([
            'email' => 'company@onlineshop.com'
        ]);

        $company2 = factory(Company::class)->create([
            'email' => 'company2@onlineshop.com'
        ]);

        $company_role = factory(Role::class)->create([
            'name' => 'company',
            'display_name' => 'Company'
        ]);

        $roleSuperRepo = new RoleRepository($company_role);
        $roleSuperRepo->attachToPermission($createProductPerm);
        $roleSuperRepo->attachToPermission($viewProductPerm);
        $roleSuperRepo->attachToPermission($updateProductPerm);
        $roleSuperRepo->attachToPermission($deleteProductPerm);

        $company->roles()->save($company_role);
        $company2->roles()->save($company_role);
    }
}
