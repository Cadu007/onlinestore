<?php

use App\Mall\Product;
use Illuminate\Database\Seeder;

class MallProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Product::create([

             'name' => 'test',
             'cover' => 'test',
             'price'=>'22',
             'description'=>'test',
             'store_id'=>'1'
             
         ])->save();
        
        
    }
}
