<?php

use App\Mall\Mall;
use Illuminate\Database\Seeder;

class MallTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('mall_malls')->insert([
            'name' => 'test',
            'location' => 'test',
            'open_time'=>'1:1:1',
            'close_time'=>'2:2:2',
            'map'=>'test',
            'cover'=>'test',
            'country_id'=>'1',
            'city'=>'Alex'

        ]);
        
    }
}
