<?php

use App\Mall\Store;
use Illuminate\Database\Seeder;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $store = Store::create([
            'name' => 'test',
            'email' => 'test@test.com',
            'password'=>bcrypt('secret123'),
            'cover'=>'test',
            'mobile'=>'test',
            'open_time'=>'2:2:2',
            'close_time'=>'2:2:2',
            'category_id'=>'1',
            'location'=>'test',
            'approved'=>'1'
        ]);
        $store->save();

        $store->malls()->sync([1]);
    }
}
