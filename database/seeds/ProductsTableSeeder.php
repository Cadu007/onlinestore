<?php

use App\Auction\ProductImages;
use App\Shop\Products\Product;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $product = factory(Product::class)->create();
        $file = UploadedFile::fake()->image('category.png', 600, 600);
        DB::table('product_images')->insert(['product_id'=>1,'src' => $file->store('products', ['disk' => 'public'])]);
    }
}