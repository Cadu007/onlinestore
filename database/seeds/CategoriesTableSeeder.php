<?php

use App\Shop\Categories\Category;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        factory(Category::class)->create();

        $data = [
            'name' => 'First',
            'slug' => str_slug('first'),
            'status' => 1,
            'f_color' => '0xFFFF5733',
            'l_color' => '0xFF3640E9',
            'icon_code' => '0xe85d',
            'icon_font' => 'MaterialIcons',
        ];

        $parent = new Category($data);

        $parent->makeRoot()->save();

        $file = UploadedFile::fake()->image('category.png', 600, 600);

        $data2 = [
            'name' => 'Second',
            'slug' => str_slug('Second'),
            'cover' => $file->store('categories', ['disk' => 'public']),
            'status' => 1,
            'f_color' => '0xFFFF5733',
            'l_color' => '0xFF3640E9',
            'icon_code' => '0xe85d',
            'icon_font' => 'MaterialIcons',
        ];

        $child = new Category( $data2 );

        $child->appendToNode($parent)->save();

        $child->attributes()->sync([1,2]);
    }
}